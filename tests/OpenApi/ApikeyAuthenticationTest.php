<?php

declare(strict_types=1);

namespace App\Tests\OpenApi;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use App\Entity\ApiKey;
use App\Repository\ApiKeyRepository;
use Hautelook\AliceBundle\PhpUnit\ReloadDatabaseTrait;

/**
 * Class ApikeyAuthenticationTest
 *
 * @group functional
 */
class ApikeyAuthenticationTest extends ApiTestCase
{
    public function testAuthenticationSucceed(): void
    {
        $client = self::createClient();
        $usage = static::getContainer()->get(ApiKeyRepository::class)->findOneBy(['usage' => 'End to End testing'])->getUsage();

        $response = $client->request('POST', '/api_token', [
            'headers' => ['Content-Type' => 'application/json'],
            'json'    => [
                'usage' => $usage,
                'token' => 'foo',
            ],
        ]);

        $this->assertResponseIsSuccessful();
        $body = $response->toArray();
        $this->assertArrayNotHasKey('refresh_token', $body);
        $token   = $body['token'];

        $client->request('GET', '/');
        $this->assertResponseStatusCodeSame(401);

        $client->request('GET', '/', ['auth_bearer' => "$token"]);
        $this->assertResponseIsSuccessful();
    }

    public function testAuthenticationFails(): void
    {
        $client = self::createClient();
        $usage = static::getContainer()->get(ApiKeyRepository::class)->findOneBy(['usage' => 'End to End testing'])->getUsage();

        $response = $client->request('POST', '/api_token', [
            'headers' => ['Content-Type' => 'application/json'],
            'json'    => [
                'usage'    => $usage,
                'token' => 'some-wrong-password',
            ],
        ]);

        $this->assertResponseStatusCodeSame(401);
        $this->assertStringContainsString('Invalid credentials', $response->getContent(false));
    }
}
