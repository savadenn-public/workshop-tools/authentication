<?php

declare(strict_types=1);

namespace App\Tests\OpenApi;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;

/**
 * Class OpenApiTest
 *
 * @group functional
 */
class OpenApiTest extends ApiTestCase
{
    /**
     * Smoke test documentation
     */
    public function testGetDoc(): void
    {
        $client = static::createClient();

        $client->request('GET', '/docs', [
            'headers' => ['Accept' => 'text/html'],
        ]);

        $this->assertResponseIsSuccessful();
    }
}
