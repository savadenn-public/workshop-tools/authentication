<?php

declare(strict_types=1);

namespace App\Tests\DataTransformer;

use ApiPlatform\Core\Validator\ValidatorInterface;
use App\DataTransformer\PasswordChangeInputDataTransformer;
use App\Dto\PasswordChange;
use App\Entity\User;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasher;

/**
 * Class DataTransformerTest
 */
class PasswordChangeInputDataTransformerTest extends TestCase
{
    /**
     * @var MockObject|ValidatorInterface
     */
    private MockObject|ValidatorInterface $validatorMock;

    /**
     * @var MockObject|UserPasswordHasher
     */
    private MockObject|UserPasswordHasher $hasher;

    /**
     * The data transformer to test
     *
     * @var PasswordChangeInputDataTransformer
     */
    private PasswordChangeInputDataTransformer $transformer;

    /**
     * @inheritDoc
     */
    protected function setUp(): void
    {
        $this->validatorMock = $this->createMock(ValidatorInterface::class);
        $this->hasher        = $this->createMock(UserPasswordHasher::class);
        $this->transformer   = new PasswordChangeInputDataTransformer($this->validatorMock, $this->hasher);
    }

    /**
     * @return array[]
     */
    public function supportProvider(): array
    {
        $context = [
            'input' => ['class' => PasswordChange::class],
        ];

        return [
            [new User(), '', [], false],                               // Already converted data
            [null, '', [], false],                                     // Empty context
            [[], User::class, ['input' => ['class' => 'foo']], false], // Invalid context class
            [[], User::class, $context, true],                         // Valid use case
        ];
    }

    /**
     * @dataProvider supportProvider
     *
     * @param mixed  $data
     * @param string $to
     * @param array  $context
     * @param bool   $expected
     */
    public function testSupportsTransformation(mixed $data, string $to, array $context, bool $expected): void
    {
        $actual = $this->transformer->supportsTransformation($data, $to, $context);

        $this->assertEquals($expected, $actual);
    }

    /**
     * Assert transformation succeed.
     */
    public function testTransform(): void
    {
        $hashedPassword = 'hashed password!!';

        // Object to populate
        $user = new User();

        // Expected result
        $expected = (new User())->setPassword($hashedPassword);

        // The hydrated DTO object used for transformation
        $data              = new PasswordChange();
        $data->newPassword = 'foo';

        // Expect validation to be done successfully
        $this->validatorMock
            ->expects($this->once())
            ->method('validate')
            ->with($data);

        // Expect hasher to hash new password
        $this->hasher
            ->expects($this->once())
            ->method('hashPassword')
            ->with($user, $data->newPassword)
            ->willReturn($hashedPassword);

        $this->transformer->transform($data, User::class, ['object_to_populate' => $user]);

        $this->assertEquals($expected, $user);
    }
}
