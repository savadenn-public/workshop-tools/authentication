<?php

declare(strict_types=1);

namespace App\Tests\EventListener;

use App\Entity\ApiKey;
use App\Entity\User;
use App\EventListener\AttachRefreshTokenOnSuccessListener;
use Gesdinet\JWTRefreshTokenBundle\EventListener\AttachRefreshTokenOnSuccessListener as Decorated;
use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationSuccessEvent;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Response;

/**
 * Test class of AttachRefreshTokenOnSuccessListener::class
 */
class AttachRefreshTokenOnSuccessListenerTest extends TestCase
{
    private Decorated|MockObject $decorated;
    private AttachRefreshTokenOnSuccessListener $listener;

    protected function setUp(): void
    {
        $this->decorated = $this->createMock(Decorated::class);
        $this->listener  = new AttachRefreshTokenOnSuccessListener($this->decorated);
    }

    /**
     * Expect decorated service to be called.
     */
    public function testAttachRefreshToken(): void
    {
        $event = new AuthenticationSuccessEvent([], new User(), new Response());

        $this->decorated
            ->expects($this->once())
            ->method('attachRefreshToken')
            ->with($event);

        $this->listener->attachRefreshToken($event);
    }

    /**
     * Expect decorated service to be bypassed.
     */
    public function testDontAttachRefreshToken(): void
    {
        $event = new AuthenticationSuccessEvent([], new ApiKey(), new Response());

        $this->decorated
            ->expects($this->never())
            ->method('attachRefreshToken');

        $this->listener->attachRefreshToken($event);
    }
}
