<?php

declare(strict_types=1);

namespace App\Tests\DataPersister;

use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use App\ConfirmEmail\ConfirmEmailHelper;
use App\DataPersister\UserDataPersister;
use App\Entity\User;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Test class of service UserDataPersister.
 */
class UserDataPersisterTest extends TestCase
{
    private MockObject|ContextAwareDataPersisterInterface $decorated;
    private MockObject|UserDataPersister $persister;
    private MockObject|Security $security;
    private MockObject|MailerInterface $mailer;

    public function setUp(): void
    {
        $this->mailer    = $this->createMock(MailerInterface::class);
        $helper          = $this->createMock(ConfirmEmailHelper::class);
        $translator      = $this->createMock(TranslatorInterface::class);
        $this->decorated = $this->createMock(ContextAwareDataPersisterInterface::class);
        $this->security  = $this->createMock(Security::class);

        // Mock translator
        $translator
            ->expects($this->any())
            ->method('trans')
            ->willReturn('Translated string');

        $this->persister = new UserDataPersister($this->decorated, $this->mailer, $helper, $translator, $this->security);
    }

    /**
     * Test method supports() expecting decorated one to be called
     */
    public function testSupports(): void
    {
        $data = [];

        $this->decorated->expects($this->once())
            ->method('supports')
            ->with($data, []);

        $this->persister->supports($data);
    }

    /**
     * Test method remove() expecting decorated one to be called
     */
    public function testRemove(): void
    {
        $data = [];

        $this->decorated->expects($this->once())
            ->method('remove')
            ->with($data, []);

        $this->persister->remove($data);
    }

    /**
     * Test changing user email address expecting rollback & confirmation email to be sent
     */
    public function testPersist(): void
    {
        $data = new User();
        $data->setEmail('bar@foo.com');

        // User email changed during a PATCH
        $context = [
            'item_operation_name' => 'patch',
            'previous_data'       => (new User())->setEmail('foo@bar.com'),
        ];

        // Expect user to NOT be granted admin
        $this->security
            ->expects($this->once())
            ->method('isGranted')
            ->with('ROLE_ADMIN')
            ->willReturn(false);

        // Expect confirmation link to be sent
        $this->mailer->expects($this->once())->method('send');

        $this->persister->persist($data, $context);

        // Expect user email to be reset
        $this->assertEquals('foo@bar.com', $data->getEmail());
    }

    /**
     * Test admin changes someone else email address expecting no confirmation email
     */
    public function testAdminPersist(): void
    {
        $data = new User();
        $data->setEmail('bar@foo.com');

        // User email changed during a PATCH
        $context = [
            'item_operation_name' => 'patch',
            'previous_data'       => (new User())->setEmail('foo@bar.com'),
        ];

        // Expect current user to be different from $data
        $this->security
            ->expects($this->once())
            ->method('getUser')
            ->willReturn(new User());

        // Expect current user to be an admin
        $this->security
            ->expects($this->once())
            ->method('isGranted')
            ->with('ROLE_ADMIN')
            ->willReturn(true);

        // Expect confirmation link to NOT be sent
        $this->mailer->expects($this->never())->method('send');

        $this->persister->persist($data, $context);
    }

    /**
     * Test admin changes his own email address expecting confirmation email to be sent
     */
    public function testAdminPersistOwn(): void
    {
        $data = new User();
        $data->setEmail('bar@foo.com');

        // User email changed during a PATCH
        $context = [
            'item_operation_name' => 'patch',
            'previous_data'       => (new User())->setEmail('foo@bar.com'),
        ];

        // Expect current user to be $data
        $this->security
            ->expects($this->once())
            ->method('getUser')
            ->willReturn($data);

        // Expect current user to be an admin
        $this->security
            ->expects($this->once())
            ->method('isGranted')
            ->with('ROLE_ADMIN')
            ->willReturn(true);

        // Expect confirmation link to be sent
        $this->mailer->expects($this->once())->method('send');

        $this->persister->persist($data, $context);
    }
}
