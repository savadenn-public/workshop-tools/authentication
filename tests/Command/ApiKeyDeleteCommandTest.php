<?php

declare(strict_types=1);

namespace App\Tests\Command;

use App\Command\ApikeyDeleteCommand;
use App\Entity\ApiKey;
use App\Repository\ApiKeyRepository;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Exception\RuntimeException;
use Symfony\Component\Console\Tester\CommandTester;

/**
 * Class ApiKeyDeleteCommandTest
 */
class ApiKeyDeleteCommandTest extends TestCase
{
    private EntityManagerInterface $manager;

    public function setUp(): void
    {
        parent::setUp();
        $this->manager = $this->createMock(EntityManagerInterface::class);
    }

    /**
     * Test execution with mandatory argument missing expecting failure
     */
    public function testExecutionMissingArguments(): void
    {
        $this->expectException(RuntimeException::class);
        $this->expectExceptionMessage('Not enough arguments (missing: "id")');

        $command = new ApikeyDeleteCommand($this->manager);
        $tester = new CommandTester($command);

        $tester->execute([]);
    }

    /**
     * Test execution succeeding
     */
    public function testExecutionSucceed(): void
    {
        $apiKey = $this->createMock(ApiKey::class);
        $repository = $this->createMock(ApiKeyRepository::class);

        $repository->expects($this->once())
            ->method('findOneBy')
            ->with(['id' => 'some-api-key-id'])
            ->willReturn($apiKey);

        $this->manager->expects($this->once())
            ->method('getRepository')
            ->with(ApiKey::Class)
            ->willReturn($repository);

        $this->manager->expects($this->once())
            ->method('remove')
            ->with($apiKey);

        $this->manager->expects($this->once())->method('flush');

        $command = new ApikeyDeleteCommand($this->manager);
        $tester = new CommandTester($command);

        $code = $tester->execute(['id' => 'some-api-key-id']);

        $this->assertEquals(0, $code, $tester->getDisplay());
        $this->assertStringContainsString('API Key revoked successfully', $tester->getDisplay());
    }
}
