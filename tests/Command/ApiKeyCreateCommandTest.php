<?php

declare(strict_types=1);

namespace App\Tests\Command;

use ApiPlatform\Core\Validator\ValidatorInterface;
use App\Command\ApikeyCreateCommand;
use App\Entity\ApiKey;
use App\Repository\ApiKeyRepository;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Exception\RuntimeException;
use Symfony\Component\Console\Tester\CommandTester;

/**
 * Class UserCreateCommandTest
 */
class ApiKeyCreateCommandTest extends TestCase
{
    private EntityManagerInterface $manager;
    private ValidatorInterface $validator;
    private ApiKeyRepository $repository;

    public function setUp(): void
    {
        parent::setUp();
        $this->manager    = $this->createMock(EntityManagerInterface::class);
        $this->validator  = $this->createMock(ValidatorInterface::class);
        $this->repository = $this->createMock(ApiKeyRepository::class);

        $this->manager
            ->expects($this->any())
            ->method('getRepository')
            ->with(ApiKey::class)
            ->willReturn($this->repository);
    }

    /**
     * Test execution with mandatory argument missing expecting failure
     */
    public function testExecutionMissingArguments(): void
    {
        $this->expectException(RuntimeException::class);
        $this->expectExceptionMessage('Not enough arguments (missing: "usage")');

        $command = new ApikeyCreateCommand($this->manager, $this->validator);
        $tester  = new CommandTester($command);

        $tester->execute([]);
    }

    /**
     * Test execution expecting success
     */
    public function testExecutionSucceed(): void
    {
        $expected = (new ApiKey())->setUsage('testExecutionSucceed');
        $expected->setCredentials('hashed_pwd', 'hashed_pwd');

        $this->repository
            ->expects($this->once())
            ->method('createApiKey')
            ->with('testExecutionSucceed')
            ->willReturn($expected);

        $this->manager->expects($this->once())->method('flush');
        $this->manager
            ->expects($this->once())
            ->method('persist')
            ->with($expected);

        $command = new ApikeyCreateCommand($this->manager, $this->validator);
        $tester  = new CommandTester($command);

        $code = $tester->execute(['usage' => 'testExecutionSucceed']);

        $this->assertEquals(0, $code, $tester->getDisplay());
        $this->assertStringContainsString('ApiKey created successfully', $tester->getDisplay());
    }

    /**
     * Test execution expecting success using a token
     */
    public function testExecutionWithTokenSucceed(): void
    {
        $expected = (new ApiKey())->setUsage('testExecutionSucceed');
        $expected->setCredentials('hashed_pwd', 'hashed_pwd');

        $this->repository
            ->expects($this->once())
            ->method('createApiKey')
            ->with('testExecutionSucceed', 'givenToken')
            ->willReturn($expected);

        $this->manager->expects($this->once())->method('flush');
        $this->manager
            ->expects($this->once())
            ->method('persist')
            ->with($expected);

        $command = new ApikeyCreateCommand($this->manager, $this->validator);
        $tester  = new CommandTester($command);

        $code = $tester->execute(['usage' => 'testExecutionSucceed', '--token' => 'givenToken']);

        $this->assertEquals(0, $code, $tester->getDisplay());
        $this->assertStringContainsString('ApiKey created successfully', $tester->getDisplay());
    }
}
