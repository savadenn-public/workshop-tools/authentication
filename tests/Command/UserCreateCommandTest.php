<?php

declare(strict_types=1);

namespace App\Tests\Command;

use ApiPlatform\Core\Validator\Exception\ValidationException;
use ApiPlatform\Core\Validator\ValidatorInterface;
use App\Command\UserCreateCommand;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Exception\RuntimeException;
use Symfony\Component\Console\Tester\CommandTester;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasher;

/**
 * Class UserCreateCommandTest
 */
class UserCreateCommandTest extends TestCase
{
    private EntityManagerInterface $manager;
    private ValidatorInterface $validator;
    private UserPasswordHasher $hasher;

    public function setUp(): void
    {
        parent::setUp();
        $this->manager   = $this->createMock(EntityManagerInterface::class);
        $this->validator = $this->createMock(ValidatorInterface::class);
        $this->hasher    = $this->createMock(UserPasswordHasher::class);
    }

    /**
     * Test execution with mandatory argument missing expecting failure
     */
    public function testExecutionMissingArguments(): void
    {
        $this->expectException(RuntimeException::class);
        $this->expectExceptionMessage('Not enough arguments (missing: "email")');

        $this->hasher->expects($this->never())->method('hashPassword');

        $command = new UserCreateCommand($this->manager, $this->validator, $this->hasher);
        $tester  = new CommandTester($command);

        $tester->execute([]);
    }

    /**
     * Test execution while exception thrown expecting failure
     */
    public function testExecutionDuplicateEmail(): void
    {

        $this->validator->expects($this->once())
            ->method('validate')
            ->willThrowException(new ValidationException('Duplicate email'));

        $command = new UserCreateCommand($this->manager, $this->validator, $this->hasher);
        $tester  = new CommandTester($command);

        $code = $tester->execute(['email' => 'foo@bar.fr']);

        $this->assertEquals(1, $code);
        $this->assertStringContainsString('Duplicate email', $tester->getDisplay());
    }

    /**
     * Test execution expecting success
     */
    public function testExecutionSucceed(): void
    {
        $expectedUser = (new User())->setEmail('foo@bar.com');
        $expectedUser->setPassword('hashed_pwd');

        $this->hasher->expects($this->once())
            ->method('hashPassword')
            ->willReturn('hashed_pwd');

        $this->manager->expects($this->once())->method('flush');
        $this->manager
            ->expects($this->once())
            ->method('persist')
            ->with($expectedUser);

        $command = new UserCreateCommand($this->manager, $this->validator, $this->hasher);
        $tester  = new CommandTester($command);

        $code = $tester->execute(['email' => 'foo@bar.com']);

        $this->assertEquals(0, $code, $tester->getDisplay());
        $this->assertStringContainsString('User created', $tester->getDisplay());
    }

    /**
     * Test execution expecting success
     */
    public function testExecutionSucceedWithPassword(): void
    {
        $expectedUser = (new User())->setEmail('foo@bar.com');
        $expectedUser->setPassword('hashed_pwd');

        $this->hasher->expects($this->once())
            ->method('hashPassword')
            ->with($this->isInstanceOf(User::class), 'foobar')
            ->willReturn('hashed_pwd');

        $this->manager->expects($this->once())->method('flush');
        $this->manager
            ->expects($this->once())
            ->method('persist')
            ->with($expectedUser);

        $command = new UserCreateCommand($this->manager, $this->validator, $this->hasher);
        $tester  = new CommandTester($command);

        $code = $tester->execute(['email' => 'foo@bar.com', '--password' => 'foobar']);

        $this->assertEquals(0, $code, $tester->getDisplay());
        $this->assertStringContainsString('User created', $tester->getDisplay());
    }

    /**
     * Test execution to create an admin
     */
    public function testExecutionSucceedWithAdmin(): void
    {
        $expectedUser = (new User())->setEmail('foo@bar.com');
        $expectedUser->setRoles(['ROLE_ADMIN']);

        $this->manager->expects($this->once())->method('flush');
        $this->manager
            ->expects($this->once())
            ->method('persist')
            ->with($expectedUser);

        $command = new UserCreateCommand($this->manager, $this->validator, $this->hasher);
        $tester  = new CommandTester($command);

        $code = $tester->execute(['email' => 'foo@bar.com', '-a' => true]);

        $this->assertEquals(0, $code, $tester->getDisplay());
        $this->assertStringContainsString('User created', $tester->getDisplay());
    }
}
