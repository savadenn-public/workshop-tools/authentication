<?php

declare(strict_types=1);

namespace App\Tests\Controller;

use Symfony\Bridge\Twig\Mime\TemplatedEmail;

/**
 * Class ResetPasswordControllerTest
 *
 * @group functional
 */
class ResetPasswordControllerTest extends AbstractApiTestCase
{
    /**
     * Test reset-password endpoint with empty request expecting failure
     */
    public function testRequestNoBody(): void
    {
        $client   = static::createClient();
        $response = $client->request('POST', '/reset_password', ['headers' => [
            'Accept'       => 'application/json',
            'Content-Type' => 'application/json',
        ], ]);

        $actual = $response->toArray(false)['detail'] ?? '';

        $this->assertResponseStatusCodeSame(400);
        $this->assertEquals('Request body is empty.', $actual);
    }

    /**
     * Test reset-password endpoint with empty email expecting failure
     */
    public function testRequestEmptyMail(): void
    {
        $client   = static::createClient();
        $response = $client->request('POST', '/reset_password', [
            'json'    => ['email' => ''],
            'headers' => [
                'Accept'       => 'application/json',
                'Content-Type' => 'application/json',
            ],
        ]);

        $actual = $response->toArray(false)['detail'] ?? '';

        $this->assertResponseStatusCodeSame(422);
        $this->assertEquals('This value should not be blank.', $actual);
    }

    /**
     * Test reset-password endpoint with an invalid email expecting failure
     */
    public function testRequestNotAnEmail(): void
    {
        $client   = static::createClient();
        $response = $client->request('POST', '/reset_password', [
            'json'    => ['email' => 'foo'],
            'headers' => [
                'Accept'       => 'application/json',
                'Content-Type' => 'application/json',
            ],
        ]);

        $actual = $response->toArray(false)['detail'] ?? '';

        $this->assertResponseStatusCodeSame(422);
        $this->assertEquals('This value is not a valid email address.', $actual);
    }

    /**
     * Test password recovery with an unknown email expecting success but no mail to be sent
     */
    public function testRequestUnknownEmail(): void
    {
        $client   = static::createClient();
        $client->request('POST', '/reset_password', [
            'json'    => ['email' => 'foo@bar.com'],
            'headers' => [
                'Accept'       => 'application/json',
                'Content-Type' => 'application/json',
            ],
        ]);

        // Expect success response but no mail to be sent
        $this->assertResponseStatusCodeSame(202);
        $this->assertEmailCount(0);
    }

    /**
     * Test password reset with a new password being too short expecting failure
     */
    public function testResetTooShortPassword(): void
    {
        static::createClient()->request('POST', '/reset_password/dummytoken', [
            'json' => ['password' => 'a'],
        ]);

        $this->assertResponseStatusCodeSame(422);
        $this->assertJsonContains(['detail' => 'This value is too short. It should have 6 characters or more.']);
    }

    /**
     * Test recovery password with an invalid token expecting bad request
     */
    public function testRecoverPasswordWithInvalidToken(): void
    {
        $client   = static::createClient();
        $client->request('POST', '/reset_password/dummytoken', [
            'json'    => ['password' => 'n3w p4$$word'],
        ]);

        $this->assertResponseStatusCodeSame(400);
    }

    /**
     * Test password recovery flow expecting success
     */
    public function testPasswordRecoveryFlow(): void
    {
        // Step 1 - Submit password recovery request expecting success & email to be sent
        $client = static::createClient();
        $client->request('POST', '/reset_password', [
            'json'    => ['email' => 'bob@example.com'],
        ]);
        $this->assertResponseStatusCodeSame(202);
        $this->assertEmailCount(1);

        // Step 2 - Extract token from sent email
        /** @var TemplatedEmail $message */
        $message = $this->getMailerMessage();
        $token = $message->getContext()['resetToken'];

        $client->request('GET', '/');
        $this->assertResponseStatusCodeSame(401);

        // Step 3 - Consume token expecting password changed
        $client = static::createClient();
        $client->request('POST', '/reset_password/'.$token->getToken(), [
            'json'    => ['password' => 'n3w p4$$word'],
        ]);

        $this->assertResponseIsSuccessful();

        // Assert authentication succeed with new credentials
        static::createAuthenticatedClient(password: 'n3w p4$$word');
        $this->assertResponseIsSuccessful();
    }
}
