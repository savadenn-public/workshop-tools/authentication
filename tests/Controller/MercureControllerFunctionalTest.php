<?php

declare(strict_types=1);

namespace App\Tests\Controller;

/**
 * Functional test of \App\Controller\MercureController::class
 *
 * @group functional
 */
class MercureControllerFunctionalTest extends AbstractApiTestCase
{
    /**
     * Smoke test MercureController::getJwt()
     */
    public function testGetJwt(): void
    {
        $client   = static::createAuthenticatedClient();
        $response = $client->request('GET', '/mercure/discovery');

        $this->assertResponseIsSuccessful();
        $this->assertArrayHasKey('set-cookie', $response->getHeaders());
    }
}
