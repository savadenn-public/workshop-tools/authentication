<?php

declare(strict_types=1);

namespace App\Tests\Controller;

use App\Controller\LogoutController;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Gesdinet\JWTRefreshTokenBundle\Entity\RefreshToken;
use Gesdinet\JWTRefreshTokenBundle\Entity\RefreshTokenRepository;
use Gesdinet\JWTRefreshTokenBundle\Model\RefreshTokenManagerInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Http\Authenticator\Token\PostAuthenticationToken;

/**
 * Class LogoutControllerTest
 */
class LogoutControllerTest extends TestCase
{

    private MockObject $tokenStorage;
    private MockObject $manager;
    private MockObject $em;
    private MockObject $tokenRepository;
    private LogoutController $controller;

    protected function setUp(): void
    {
        $this->tokenStorage    = $this->createMock(TokenStorageInterface::class);
        $this->manager         = $this->createMock(RefreshTokenManagerInterface::class);
        $this->em              = $this->createMock(EntityManagerInterface::class);
        $this->tokenRepository = $this->createMock(RefreshTokenRepository::class);
        $this->controller      = new LogoutController($this->tokenStorage, $this->manager, $this->em);

        $this->em
            ->expects($this->any())
            ->method('getRepository')
            ->willReturn($this->tokenRepository);
    }

    /**
     * Test logout route expecting success
     */
    public function testLogoutSuccess(): void
    {
        $user = $this->createMock(User::class);
        $user->expects($this->once())->method('getId')->willReturn('foo-uuid');

        // Expect user to be logged in
        $this->tokenStorage
            ->expects($this->once())
            ->method('getToken')
            ->willReturn(new PostAuthenticationToken($user, 'main', ['ROLE_USER']));

        // Expect repository to find a matching result
        $refreshToken = new RefreshToken();
        $this->tokenRepository
            ->expects($this->once())
            ->method('findOneBy')
            ->with([
                'username'     => 'foo-uuid',
                'refreshToken' => 'foo_token_value',
            ])
            ->willReturn($refreshToken);

        // Expect manager to delete found refresh token
        $this->manager
            ->expects($this->once())
            ->method('delete')
            ->with($refreshToken);

        $request = Request::create('/logout', 'POST', content: '{"refresh_token":"foo_token_value"}');

        $actual = $this->controller->revokeRefreshToken($request);

        $this->assertInstanceOf(JsonResponse::class, $actual);
        $this->assertEquals(204, $actual->getStatusCode());
    }

    /**
     * Test logout with missing parameter "refresh_token" in the request expecting failure
     */
    public function testLogoutEmptyRequest(): void
    {
        $this->expectException(BadRequestException::class);

        $request = Request::create('/logout', 'POST', content: '{}');
        $this->controller->revokeRefreshToken($request);
    }

    /**
     * Test logout with an unknown refresh token expecting failure
     */
    public function testLogoutUnknownToken(): void
    {
        $this->expectException(BadRequestException::class);

        $user = $this->createMock(User::class);
        $user->expects($this->once())->method('getId')->willReturn('foo-uuid');

        // Expect user to be logged in
        $this->tokenStorage
            ->expects($this->once())
            ->method('getToken')
            ->willReturn(new PostAuthenticationToken($user, 'main', ['ROLE_USER']));

        // Expect repository to not find a matching refresh token
        $this->tokenRepository
            ->expects($this->once())
            ->method('findOneBy')
            ->willReturn(null);

        $request = Request::create('/logout', 'POST', content: '{"refresh_token": ""}');
        $this->controller->revokeRefreshToken($request);
    }
}
