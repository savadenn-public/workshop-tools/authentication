<?php

declare(strict_types=1);

namespace App\Tests\Controller;

use Hautelook\AliceBundle\PhpUnit\ReloadDatabaseTrait;

/**
 * Class LogoutFlowTest
 *
 * @group functional
 */
class LogoutFlowTest extends AbstractApiTestCase
{
    use ReloadDatabaseTrait;

    /**
     * Test logout flow
     */
    public function testLogout(): void
    {
        $client = static::createClient();
        // Log user in
        $response = $client->request('POST', '/authentication_token', [
            'headers' => ['Content-Type' => 'application/json'],
            'json'    => [
                'email'    => 'bob@example.com',
                'password' => 'foo',
            ],
        ]);

        $data = $response->toArray();
        $client->setDefaultOptions(['auth_bearer' => $data['token']]);

        // Then, log out
        $client->request('POST', '/logout', ['json' => ['refresh_token' => $data['refresh_token']]]);

        $this->assertResponseIsSuccessful();
    }
}
