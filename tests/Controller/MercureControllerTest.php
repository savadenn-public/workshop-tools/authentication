<?php

declare(strict_types=1);

namespace App\Tests\Controller;

use App\Controller\MercureController;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWSProvider\JWSProviderInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Signature\LoadedJWS;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mercure\Jwt\TokenFactoryInterface;

/**
 * Test class of MercureController::class
 */
class MercureControllerTest extends TestCase
{
    private MockObject $factory;
    private MockObject $jwsProvider;
    private MockObject $logger;
    private MercureController $controller;

    /**
     * Test method getJwt() with cookie "mercureAuthorization" already set expecting no-content
     */
    public function testJwtAlreadyExists(): void
    {
        $request = Request::create(uri: '/', cookies: ['mercureAuthorization' => 'valid.jwt.value']);

        // Create a still-valid JWT
        $jwt = new LoadedJWS(['exp' => time()+10], true);

        $this->jwsProvider
            ->expects($this->once())
            ->method('load')
            ->willReturn($jwt);

        $this->factory
            ->expects($this->never())
            ->method('create');

        $expected = new Response(status: 204);
        $actual   = $this->controller->getJwt($request);

        $this->assertEquals($expected, $actual);
    }

    /**
     * Test method getJwt() with cookie "mercureAuthorization" set with an invalid value expecting new JWT to be created
     */
    public function testJwtException(): void
    {
        $request = Request::create(uri: '/', cookies: ['mercureAuthorization' => 'invalid.jwt.value']);

        // Expect JWS provider to throw an exception
        $this->jwsProvider
            ->expects($this->once())
            ->method('load')
            ->willThrowException(new \Exception('Invalid JWT format'));

        // Expect logger to log error
        $this->logger
            ->expects($this->once())
            ->method('error')
            ->with('Error while trying to decode Mercure JWT: Invalid JWT format');

        // Expect factory to create a new JWT
        $this->factory
            ->expects($this->once())
            ->method('create')
            ->with(['*'])
            ->willReturn('dummy.jwt.value');

        $this->controller->getJwt($request);
    }

    /**
     * Test method getJwt() without any cookie expecting new JWT to be created
     */
    public function testGetJwt(): void
    {
        $this->factory
            ->expects($this->once())
            ->method('create')
            ->with(['*'])
            ->willReturn('dummy.jwt.value');

        $request = Request::create('/');
        $actual  = $this->controller->getJwt($request);

        $expected = new Response();
        $expected->headers->setCookie(Cookie::create(name: 'mercureAuthorization', value: 'dummy.jwt.value', secure: true, httpOnly: true));

        $this->assertEquals($expected, $actual);
    }

    protected function setUp(): void
    {
        $this->factory     = $this->createMock(TokenFactoryInterface::class);
        $this->jwsProvider = $this->createMock(JWSProviderInterface::class);
        $this->logger      = $this->createMock(LoggerInterface::class);

        $this->controller = new MercureController($this->factory, $this->jwsProvider, $this->logger);
    }
}
