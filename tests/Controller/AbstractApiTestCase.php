<?php

declare(strict_types=1);

namespace App\Tests\Controller;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\Client;

/**
 * Class AbstractApiTestCase
 */
abstract class AbstractApiTestCase extends ApiTestCase
{
    /**
     * Create an HTTP client having a valid JWT set in auth header.
     * The default user used for authentication is Bob (@see fixtures/users.yaml)
     *
     * @param string $email    Email of the user
     * @param string $password Password of the user
     *
     * @return Client
     */
    protected function createAuthenticatedClient(string $email = 'bob@example.com', string $password = 'foo'): Client
    {
        $client = static::createClient();
        $response = $client->request('POST', '/authentication_token', [
            'headers' => ['Content-Type' => 'application/json'],
            'json'    => [
                'email'    => $email,
                'password' => $password,
            ],
        ]);

        $data = $response->toArray();
        $client->setDefaultOptions(['auth_bearer' => $data['token']]);

        return $client;
    }
}
