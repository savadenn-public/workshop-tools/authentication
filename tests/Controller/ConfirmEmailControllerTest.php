<?php

declare(strict_types=1);

namespace App\Tests\Controller;

use App\Repository\UserRepository;
use Hautelook\AliceBundle\PhpUnit\RefreshDatabaseTrait;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;

/**
 * Class ConfirmEmailControllerTest
 *
 * @group functional
 */
class ConfirmEmailControllerTest extends AbstractApiTestCase
{
    use RefreshDatabaseTrait;

    /**
     * Assert bad request on invalid token
     */
    public function testConfirmInvalidToken(): void
    {
        $client = static::createAuthenticatedClient();
        $client->request('POST', '/confirm_email', ['json' => ['token' => 'foo']]);

        $this->assertResponseStatusCodeSame(400);
    }

    /**
     * Test the full process of email changing:
     *   - Step 1: request for email change
     *   - Step 2: consume token to do change email
     *   - Step 3: credentials become invalid, have to reconnect
     */
    public function testChangeEmail(): void
    {
        $client = static::createAuthenticatedClient('paul@example.com');
        $id = static::getContainer()->get(UserRepository::class)->findOneBy(['email' => 'paul@example.com'])->getId();

        // Step 1 - request email change
        $client->request('PATCH', "/users/$id", [
            'json'    => ['email' => 'new-email@example.com'],
            'headers' => [
                'Accept'           => 'application/json',
                'Accept-Language'  => 'fr',
                'Content-Type'     => 'application/merge-patch+json',
            ],
        ]);

        $this->assertResponseStatusCodeSame(200);

        // Assert email was sent
        $this->assertEmailCount(1);

        /** @var TemplatedEmail $message */
        $message = $this->getMailerMessage();
        $token   = $message->getContext()['token'];
        $this->assertEmailAddressContains($message, 'To', 'paul@example.com', 'Email sent to the wrong address');

        $this->assertEquals('Demande de changement d’email', $message->getSubject(), 'Email subject is invalid');

        // Assert the user's email did not change yet
        $actual = $client->request('GET', "/users/$id", ['headers' => ['accept' => 'application/json']])->toArray()['email'];
        $this->assertEquals('paul@example.com', $actual);

        // Step 2 - consume token to change email
        $client->request('POST', '/confirm_email', ['json' => ['token' => $token]]);

        $this->assertResponseStatusCodeSame(200);

        // Step 3 - As a result, and because email is the user identifier, expect credentials to become invalid
        $client->request('GET', '/users');
        $this->assertResponseStatusCodeSame(401);

        // Reconnect client
        $client = static::createAuthenticatedClient('new-email@example.com');

        // Assert user's email changed
        $actual = $client->request('GET', "/users/$id", ['headers' => ['accept' => 'application/json']])->toArray()['email'];
        $this->assertEquals('new-email@example.com', $actual);
    }
}
