<?php

declare(strict_types=1);

namespace App\Tests\Entity;

use App\Entity\User;
use App\Entity\UserOrganisation;
use Doctrine\ORM\EntityManagerInterface;
use Hautelook\AliceBundle\PhpUnit\RefreshDatabaseTrait;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * Test class of User::class
 *
 * @group functional
 */
class UserTest extends KernelTestCase
{
    use RefreshDatabaseTrait;

    /**
     * Test delete a user having UserOrganisation expecting cascade deletion
     */
    public function testDeleteUserHavingOrganisations(): void
    {
        /** @var EntityManagerInterface $manager */
        $manager = static::getContainer()->get(EntityManagerInterface::class);

        // See fixtures/users.yaml
        $user = $manager->getRepository(User::class)->findOneBy(['email' => 'john@example.com']);

        // See fixtures/user_organisations.yaml
        $actual = $manager->getRepository(UserOrganisation::class)->findOneBy(['user' => $user]);

        // Expect user to have one UserOrganisation
        $this->assertInstanceOf(UserOrganisation::class, $actual);

        // Delete user
        $manager->remove($user);
        $manager->flush();

        // Expect UserOrganisation to be cascade deleted
        $actual = $manager->getRepository(UserOrganisation::class)->findOneBy(['user' => $user]);
        $this->assertEmpty($actual);
    }

    public function testAddUserOrganisation(): void
    {
        $organisation = new UserOrganisation();
        $user         = new User();

        $this->assertCount(0, $user->getUserOrganisations());

        $user->addUserOrganisation($organisation);
        $this->assertEquals($user, $organisation->getUser());
        $this->assertCount(1, $user->getUserOrganisations());
    }

    /**
     * @depends testAddUserOrganisation
     */
    public function testRemoveUserOrganisation(): void
    {
        $organisation = new UserOrganisation();
        $user         = new User();

        $user->addUserOrganisation($organisation);
        $user->removeUserOrganisation($organisation);

        $this->assertCount(0, $user->getUserOrganisations());
    }
}
