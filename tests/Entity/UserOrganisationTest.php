<?php

declare(strict_types=1);

namespace App\Tests\Entity;

use App\Entity\UserOrganisation;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * @group functional
 */
class UserOrganisationTest extends KernelTestCase
{
    /**
     * Test creating duplicate UserOrganisation expecting exception
     */
    public function testCreateDuplicate(): void
    {
        $this->expectException(UniqueConstraintViolationException::class);

        /** @var EntityManagerInterface $manager */
        $manager = static::getContainer()->get(EntityManagerInterface::class);

        $userOrganisation = $manager->getRepository(UserOrganisation::class)->findOneBy([]);

        // Create a duplicate UserOrganisation
        $model = new UserOrganisation();
        $model->setUser($userOrganisation->getUser())
            ->setOrganisation($userOrganisation->getOrganisation());

        $manager->persist($model);
        $manager->flush();
    }
}
