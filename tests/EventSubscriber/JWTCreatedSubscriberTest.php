<?php

declare(strict_types=1);

namespace App\Tests\EventSubscriber;

use App\Entity\ApiKey;
use App\Entity\Organisation;
use App\Entity\User;
use App\Entity\UserOrganisation;
use App\EventSubscriber\JWTCreatedSubscriber;
use Doctrine\Common\Collections\ArrayCollection;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTCreatedEvent;
use PHPUnit\Framework\TestCase;
use Symfony\Component\EventDispatcher\EventDispatcher;

/**
 * Class JWTCreatedListener
 */
class JWTCreatedSubscriberTest extends TestCase
{
    /**
     * Assert subscriber append user ID + user organisations in JWT payload
     */
    public function testAppendUserOrganisations(): void
    {
        $user = $this->createMock(User::class);
        $user->expects($this->once())
            ->method('getId')
            ->willReturn('237e9877-e79b-12d4-a765-321741963000');

        // One organisation user has access to
        $organisation = $this->createMock(Organisation::class);
        $organisation->expects($this->once())
            ->method('getId')
            ->willReturn('organisation-id');

        // List of organisations user has access to
        $organisations = [
            (new UserOrganisation())
                ->setOrganisation($organisation)
                ->setRoles(['ROLE_FOO']),
        ];

        // Mock user organisations
        $user->expects($this->once())
            ->method('getUserOrganisations')
            ->willReturn(new ArrayCollection($organisations));

        $dispatcher = new EventDispatcher();
        $subscriber = new JWTCreatedSubscriber();

        $dispatcher->addSubscriber($subscriber);
        $event = new JWTCreatedEvent([], $user);
        $dispatcher->dispatch($event, 'lexik_jwt_authentication.on_jwt_created');

        $actual   = $event->getData();
        $expected = [
            'guid'          => '237e9877-e79b-12d4-a765-321741963000',
            'organisations' => [
                'organisation-id' => ['ROLE_FOO'],
            ],
        ];

        $this->assertEquals($expected, $actual);
    }

    /**
     * Assert subscriber append ApiKey ID in JWT payload
     */
    public function testAppendApiKeyId(): void
    {
        $key = $this->createMock(ApiKey::class);
        $key->expects($this->once())
            ->method('getId')
            ->willReturn('api-key-id');

        $dispatcher = new EventDispatcher();
        $subscriber = new JWTCreatedSubscriber();
        $dispatcher->addSubscriber($subscriber);

        $event = new JWTCreatedEvent([], $key);
        $dispatcher->dispatch($event, 'lexik_jwt_authentication.on_jwt_created');

        $expected = [
            'guid' => 'api-key-id',
        ];

        $this->assertEquals($expected, $event->getData());
    }
}
