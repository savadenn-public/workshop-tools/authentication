<?php

declare(strict_types=1);

namespace App\Tests\Serializer;

use ApiPlatform\Core\Serializer\SerializerContextBuilderInterface;
use App\Serializer\UserContextBuilder;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

/**
 * Class UserContextBuilderTest
 */
class UserContextBuilderTest extends TestCase
{
    /**
     * Expect (de)normalization groups to be updated for admin
     */
    public function testCreateFromAdminRequest(): void
    {
        $decorated = $this->createMock(SerializerContextBuilderInterface::class);
        $decorated->expects($this->exactly(2))
            ->method('createFromRequest')
            ->willReturn([
                'groups'         => [],
                'resource_class' => 'App\Entity\User',
            ]);

        $checker = $this->createMock(AuthorizationCheckerInterface::class);
        $checker->expects($this->exactly(2))
            ->method('isGranted')
            ->with('ROLE_ADMIN')
            ->willReturn(true);

        $builder = new UserContextBuilder($decorated, $checker);

        // Normalization context
        $actual = $builder->createFromRequest(new Request(), true);
        $this->assertEquals(['admin:output'], $actual['groups'] ?? null);

        // Denormalization context
        $actual = $builder->createFromRequest(new Request(), false);
        $this->assertEquals(['admin:input'], $actual['groups'] ?? null);
    }
}
