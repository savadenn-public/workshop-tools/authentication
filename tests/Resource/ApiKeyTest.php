<?php

declare(strict_types=1);

namespace App\Tests\Resource;

use App\Repository\ApiKeyRepository;
use App\Tests\Controller\AbstractApiTestCase;
use Hautelook\AliceBundle\PhpUnit\RefreshDatabaseTrait;

/**
 * Class ApiKeyTest
 *
 * @group functional
 */
class ApiKeyTest extends AbstractApiTestCase
{
    use RefreshDatabaseTrait;

    /**
     * Retrieve API Key collection smoke test
     */
    public function testGetApiKeyCollection(): void
    {
        $client = $this->createAuthenticatedClient('alice@example.com');
        $response = $client->request('GET', '/api_keys');

        $this->assertResponseIsSuccessful();
        $this->assertEquals(2, $response->toArray()['hydra:totalItems']);
    }

    /**
     * Retrieve API Key item smoke test
     */
    public function testGetApiKeyItem(): void
    {
        $client = $this->createAuthenticatedClient('alice@example.com');
        $id = static::getContainer()->get(ApiKeyRepository::class)->findOneBy(['usage' => 'End to End testing'])->getId();

        $response = $client->request('GET', "/api_keys/$id");
        $this->assertResponseIsSuccessful();

        $this->assertEquals($response->toArray()['usage'], 'End to End testing');
        $this->assertArrayNotHasKey('plainToken', $response->toArray());
    }

    public function testCreateApiKey(): void
    {
        $client = $this->createAuthenticatedClient('alice@example.com');
        $response = $client->request('POST', "/api_keys", [
            'json' => [
                'usage' => 'foobar usage',
            ],
        ]);
        
        $this->assertResponseIsSuccessful();
        $this->assertEquals($response->toArray()['usage'], 'foobar usage');
        $this->assertNotEmpty($response->toArray()['plainToken']);
        $this->assertNotEmpty($response->toArray()['id']);
    }
}
