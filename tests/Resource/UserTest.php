<?php

declare(strict_types=1);

namespace App\Tests\Resource;

use App\Entity\User;
use App\Repository\UserRepository;
use App\Tests\Controller\AbstractApiTestCase;
use Hautelook\AliceBundle\PhpUnit\RefreshDatabaseTrait;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpClient\Exception\ClientException;

/**
 * Class UserTest
 *
 * @group functional
 */
class UserTest extends AbstractApiTestCase
{
    use RefreshDatabaseTrait;

    /**
     * Assert a user can change its own name
     */
    public function testChangeName(): void
    {
        $client = $this->createAuthenticatedClient();
        $userId = static::getContainer()->get(UserRepository::class)->findOneByEmail('bob@example.com')->getId();

        $client->request('PATCH', "/users/$userId", [
            'headers' => ['Content-Type' => 'application/merge-patch+json'],
            'json'    => [
                'name'     => 'Jedi',
                // Try to update other fields
                'email'    => 'foo@bar.com',
                'password' => 'foo123',
                'roles'    => ['ROLE_ADMIN'],
            ],
        ]);

        $this->assertResponseIsSuccessful();

        // Expected nothing else but "name" to be updated
        $expectedUser = [
            'id'    => $userId,
            'roles' => ['ROLE_USER'],
            'email' => 'bob@example.com',
            'name'  => 'Jedi',
        ];
        $actual       = $client->request('GET', "/users/$userId", [
            'headers' => ['Accept' => 'application/json'],
        ])->toArray();

        $this->assertEquals($expectedUser, $actual);
    }

    /**
     * Assert a user cannot change someone else name
     */
    public function testCannotChangeSomeoneElseName(): void
    {
        $client = $this->createAuthenticatedClient();
        $userId = static::getContainer()->get(UserRepository::class)->findOneByEmail('alice@example.com')->getId();

        $client->request('PATCH', "/users/$userId", [
            'headers' => ['Content-Type' => 'application/merge-patch+json'],
            'json'    => ['name' => 'Jedi'],
        ]);

        $this->assertResponseStatusCodeSame(403);
    }

    /**
     * Assert a non-admin user can't create a new user
     */
    public function testCantCreateUser(): void
    {
        $client = $this->createAuthenticatedClient();
        $client->request('POST', '/users', [
            'json' => [
                'email' => 'foo@example.com',
                'roles' => ['ROLE_USER'],
            ],
        ]);

        $this->assertResponseStatusCodeSame(403);
    }

    /**
     * Assert an admin can create a new user
     */
    public function testCanCreateUser(): void
    {
        // Admin user
        $client = $this->createAuthenticatedClient('alice@example.com');

        $client->request('POST', '/users', [
            'json' => [
                'email' => 'foo@example.com',
                'roles' => ['ROLE_USER'],
            ],
        ]);

        // Assert user resource was created
        $this->assertResponseStatusCodeSame(201);

        // Check that an e-mail was sent
        $this->assertEmailCount(1);

        /** @var TemplatedEmail $message */
        $message = $this->getMailerMessage();
        $this->assertEquals('Invitation on Workshop planner', $message->getSubject());
    }

    /**
     * Assert can't create a user with an existing email address
     */
    public function testCantCreateDuplicateUser(): void
    {
        // Admin user
        $client = $this->createAuthenticatedClient('alice@example.com');

        $client->request('POST', '/users', [
            'json' => [
                'email' => 'alice@example.com',
                'roles' => ['ROLE_USER'],
            ],
        ]);

        // Assert user resource was not created
        $this->assertResponseStatusCodeSame(422);
    }

    /**
     * Test invite user with invalid parameters expecting failure
     */
    public function testInviteUserFail(): void
    {
        $client = $this->createAuthenticatedClient('alice@example.com');

        $response = $client->request('POST', '/users', [
            'json' => [
                'email' => str_repeat('foo', 80).'john.doe@example.com',
                'roles' => ['ROLE_USER', 'foo'],
            ],
        ]);

        $this->expectException(ClientException::class);
        $this->expectExceptionCode(422);
        $this->expectExceptionMessage("email: This value is too long. It should have 180 characters or less.\nroles: One or more of the given values is invalid.");

        $response->toArray();
    }

    /**
     * Test invite user with an already existing email address expecting failure
     */
    public function testInviteExistingEmail(): void
    {
        $client = $this->createAuthenticatedClient('alice@example.com');

        // User tries to invite himself
        $response = $client->request('POST', '/users', [
            'json' => [
                'email' => 'alice@example.com',
                'roles' => [],
            ],
        ]);

        $this->expectException(ClientException::class);
        $this->expectExceptionCode(422);
        $this->expectExceptionMessage('email: This value is already used.');

        $response->toArray();
    }

    /**
     * Test cascade removal of a user having an avatar
     */
    public function testDelete(): void
    {
        // This user has an avatar
        $user = static::getContainer()->get(UserRepository::class)->findOneByEmail('alice@example.com');

        /** @var User $user */
        $client = $this->createAuthenticatedClient($user->getEmail());

        $client->request('DELETE', sprintf('/users/%s', $user->getId()));

        $this->assertResponseIsSuccessful();
    }
}
