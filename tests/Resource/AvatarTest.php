<?php

declare(strict_types=1);

namespace App\Tests\Resource;

use App\Tests\Controller\AbstractApiTestCase;

/**
 * @group functional
 */
class AvatarTest extends AbstractApiTestCase
{
    /**
     * Test creation of an avatar
     */
    public function testCreate(): void
    {
        $client = $this->createAuthenticatedClient();

        $client->request('POST', '/avatars', [
            'json' => [
                'hatColor'        => '#123456',
                'faceColor'       => '#123456',
                'hairColor'       => '#123456',
                'shirtColor'      => '#123456',
                'earSize'         => 'small',
                'eyeType'         => 'oval',
                'hatType'         => 'none',
                'hairType'        => 'mohawk',
                'noseType'        => 'round',
                'mouthType'       => 'smile',
                'shirtType'       => 'polo',
                'glassesType'     => 'none',
                'earring'         => 'none',
                'eyebrowType'     => 'up',
            ],
        ]);

        $this->assertResponseIsSuccessful();
    }
}
