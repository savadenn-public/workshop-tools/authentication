<?php

declare(strict_types=1);

namespace App\Tests\Security;

use App\Tests\Controller\AbstractApiTestCase;
use Symfony\Component\HttpClient\Exception\ClientException;

/**
 * Test class of UserChecker::class
 *
 * @group functional
 */
class UserCheckerTest extends AbstractApiTestCase
{
    /**
     * Unit test method checkPreAuth() with a locked user expecting HTTP 401
     */
    public function testCheckPreAuth(): void
    {
        $this->expectException(ClientException::class);
        $this->expectExceptionCode(401);
        $this->createAuthenticatedClient('emma@example.com');
    }
}
