<?php

declare(strict_types=1);

namespace App\Tests\Security\Voter;

use App\Entity\Avatar;
use App\Entity\User;
use App\Security\Voter\AvatarVoter;
use Lexik\Bundle\JWTAuthenticationBundle\Security\Authentication\Token\JWTUserToken;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManager;

/**
 * Test class of AvatarVoter
 */
class AvatarVoterTest extends TestCase
{
    /**
     * @var AccessDecisionManager
     */
    private AccessDecisionManager $decisionManager;

    /**
     * @inheritDoc
     */
    protected function setUp(): void
    {
        $this->decisionManager = new AccessDecisionManager([new AvatarVoter()]);
    }

    public function actionProvider(): array
    {
        return [
            ['PATCH'],
            ['DELETE'],
        ];
    }

    /**
     * Test user can edit his own avatar
     *
     * @param string $action
     *
     * @dataProvider actionProvider
     */
    public function testUserCanEditOwnAvatar(string $action): void
    {
        $user    = new User();
        $subject = (new Avatar())->setUser($user);
        $token   = new JWTUserToken(user: $user);

        $actual = $this->decisionManager->decide($token, [$action], $subject);
        $this->assertTrue($actual);
    }

    /**
     * Test user can't edit someone else avatar
     *
     * @param string $action
     *
     * @dataProvider actionProvider
     */
    public function testUserCantEditAvatar(string $action): void
    {
        $user    = new User();
        $subject = (new Avatar())->setUser(new User());
        $token   = new JWTUserToken(user: $user);

        $actual = $this->decisionManager->decide($token, [$action], $subject);
        $this->assertFalse($actual);
    }
}
