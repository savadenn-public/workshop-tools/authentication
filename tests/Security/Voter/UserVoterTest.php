<?php

declare(strict_types=1);

namespace App\Tests\Security\Voter;

use App\Entity\User;
use App\Security\Voter\UserVoter;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Security\Core\Authentication\Token\NullToken;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManager;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Http\Authenticator\Token\PostAuthenticationToken;

/**
 * Class UserVoterTest
 */
class UserVoterTest extends TestCase
{
    /**
     * @var AccessDecisionManager
     */
    private AccessDecisionManager $decisionManager;

    private Security|MockObject $security;

    /**
     * @inheritDoc
     */
    protected function setUp(): void
    {
        $this->security        = $this->createMock(Security::class);
        $this->decisionManager = new AccessDecisionManager([new UserVoter($this->security)]);
    }

    /**
     * Assert anonymous user cannot change someone else password
     */
    public function testAnonymousCanChangePassword(): void
    {
        $subject = new User();
        $actual  = $this->decisionManager->decide(new NullToken(), ['CHANGE_PASSWORD'], $subject);

        $this->assertFalse($actual, 'Anonymous user can change password of someone else');
    }

    /**
     * Assert authenticated user cannot change someone else password
     */
    public function testCantChangeSomeoneElsePassword(): void
    {
        $authenticatedUser = (new User())->setEmail('Bob');
        $subject           = (new User())->setEmail('Alice');
        $token             = new PostAuthenticationToken($authenticatedUser, 'main', $authenticatedUser->getRoles());

        $actual = $this->decisionManager->decide($token, ['CHANGE_PASSWORD'], $subject);

        $this->assertFalse($actual, 'Authenticated user can change password of someone else');
    }

    /**
     * Assert authenticated user can change its own password
     */
    public function testCanChangePassword(): void
    {
        $authenticatedUser = (new User())->setEmail('Bob');
        $token             = new PostAuthenticationToken($authenticatedUser, 'main', $authenticatedUser->getRoles());

        $actual = $this->decisionManager->decide($token, ['CHANGE_PASSWORD'], $authenticatedUser);

        $this->assertTrue($actual, 'Authenticated user cannot change its password');
    }

    /**
     * Assert a user cannot edit another user
     */
    public function testCantEditUser(): void
    {
        $this->security->method('isGranted')->with('ROLE_ADMIN')->willReturn(false);
        $authenticatedUser = (new User())->setEmail('Bob');
        $token             = new PostAuthenticationToken($authenticatedUser, 'main', $authenticatedUser->getRoles());
        $subject           = (new User())->setEmail('Alice');
        $actual            = $this->decisionManager->decide($token, ['USER_EDIT'], $subject);

        $this->assertFalse($actual);
    }

    /**
     * Assert an admin can edit another user
     */
    public function testAdminCanEditUser(): void
    {
        $this->security->method('isGranted')->with('ROLE_ADMIN')->willReturn(true);
        $authenticatedUser = (new User())->setEmail('Bob');
        $token             = new PostAuthenticationToken($authenticatedUser, 'main', $authenticatedUser->getRoles());
        $subject           = (new User())->setEmail('Alice');
        $actual            = $this->decisionManager->decide($token, ['USER_EDIT'], $subject);

        $this->assertTrue($actual);
    }

    /**
     * Assert authenticated user can change its own data
     */
    public function testCanEditUser(): void
    {
        $authenticatedUser = (new User())->setEmail('Bob');
        $token             = new PostAuthenticationToken($authenticatedUser, 'main', $authenticatedUser->getRoles());

        $actual = $this->decisionManager->decide($token, ['USER_EDIT'], $authenticatedUser);

        $this->assertTrue($actual);
    }
}
