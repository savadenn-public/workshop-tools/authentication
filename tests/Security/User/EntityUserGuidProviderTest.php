<?php

declare(strict_types=1);

namespace App\Tests\Security\User;

use App\Entity\User;
use App\Repository\UserRepository;
use App\Security\User\EntityUserGuidProvider;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class EntityUserGuidProviderTest
 */
class EntityUserGuidProviderTest extends TestCase
{
    private EntityManagerInterface $manager;
    private EntityUserGuidProvider $provider;

    /**
     * @inheritDoc
     */
    protected function setUp(): void
    {
        $this->manager = $this->createMock(EntityManagerInterface::class);
        $this->provider = new EntityUserGuidProvider($this->manager);
    }

    /**
     * Test method refreshUser() expecting exception as it should never be called
     */
    public function testRefreshUser(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Should never refresh user');
        $this->provider->refreshUser(new User());
    }

    /**
     * Provide test data for testSupportsClass
     *
     * @return array[]
     */
    public function classProvider(): array
    {
        return [
            [User::class, true],
            [UserInterface::class, false],
            ['anything', false],
        ];
    }

    /**
     * Test method supportClass() with several inputs
     *
     * @dataProvider classProvider
     *
     * @param string $class
     * @param bool   $expected
     */
    public function testSupportsClass(string $class, bool $expected): void
    {
        $actual = $this->provider->supportsClass($class);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test method loadUserByIdentifier() expecting fetched user
     */
    public function testLoadUserByIdentifier(): void
    {
        $expectedUser = new User();

        $repository = $this->createMock(UserRepository::class);
        $repository->expects($this->once())
            ->method('loadByGuid')
            ->with('foo-uuid')
            ->willReturn($expectedUser);

        $this->manager
            ->expects($this->once())
            ->method('getRepository')
            ->with(User::class)
            ->willReturn($repository);

        $actual = $this->provider->loadUserByIdentifier('foo-uuid');

        $this->assertSame($expectedUser, $actual);
    }
}
