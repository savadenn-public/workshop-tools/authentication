# Authentication

Authentication and User resource

* [Stable online documentation](https://savadenn-public.gitlab.io/workshop-tools/authentication)
* [Documentation source](./docs)

## Contact

You may join us on our public [Matrix room](https://matrix.to/#/#workshop-tools:savadenn.ems.host)

## Authors and contributors

Workshop Authentication is made by [Savadenn](https://www.savadenn.bzh) with the help of individual contributors.

The complete list can be found [here](https://gitlab.com/savadenn-public/workshop-tools/authentication/-/graphs/latest).
