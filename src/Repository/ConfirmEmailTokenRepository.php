<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\ConfirmEmailToken;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ConfirmEmailToken|null find($id, $lockMode = null, $lockVersion = null)
 * @method ConfirmEmailToken|null findOneBy(array $criteria, array $orderBy = null)
 * @method ConfirmEmailToken[]    findAll()
 * @method ConfirmEmailToken[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ConfirmEmailTokenRepository extends ServiceEntityRepository
{
    /**
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ConfirmEmailToken::class);
    }

    /**
     * @param User               $user
     * @param \DateTimeImmutable $expiresAt
     * @param string             $newEmail
     * @param string             $hashedToken
     *
     * @return ConfirmEmailToken
     */
    public function createResetPasswordRequest(User $user, \DateTimeImmutable $expiresAt, string $newEmail, string $hashedToken): ConfirmEmailToken
    {
        return new ConfirmEmailToken($user, $expiresAt, $newEmail, $hashedToken);
    }
}
