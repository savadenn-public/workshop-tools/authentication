<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\ApiKey;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

/**
 * @method ApiKey|null find($id, $lockMode = null, $lockVersion = null)
 * @method ApiKey|null findOneBy(array $criteria, array $orderBy = null)
 * @method ApiKey[]    findAll()
 * @method ApiKey[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ApiKeyRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry, private UserPasswordHasherInterface $hasher, private int $tokenLength = 128)
    {
        parent::__construct($registry, ApiKey::class);
    }

    /**
     * Creates a new Api Key
     *
     * BEWARE : plainToken is set and must be removed if unnecessary using ApiKey::eraseCredentials()
     * @param $usage         string
     * @param string|null $inputPassword if null, it will be generated
     *
     * @return ApiKey
     *
     * @see ApiKey::eraseCredentials()
     */
    public function createApiKey(string $usage, string $inputPassword = null) :ApiKey
    {
        $apiKey = (new ApiKey())->setUsage($usage);
        $password = $inputPassword ?? bin2hex(random_bytes($this->tokenLength));

        $apiKey->setCredentials(
            $password,
            $this->hasher->hashPassword($apiKey, $password)
        );

        return $apiKey;
    }
}
