<?php

declare(strict_types=1);

namespace App\EventSubscriber;

use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\User;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use SymfonyCasts\Bundle\ResetPassword\ResetPasswordHelperInterface;

/**
 * Send a password recovery link to new user to force him/her to choose a password
 */
class InviteUserSubscriber implements EventSubscriberInterface
{
    public function __construct(private MailerInterface $mailer, private ResetPasswordHelperInterface $helper, private TranslatorInterface $translator, private string $appName)
    {
    }

    /**
     * @param ViewEvent $event
     */
    public function onUserCreated(ViewEvent $event)
    {
        $user = $event->getControllerResult();

        if ($this->supports($event)) {
            // Generate a reset token
            $resetToken = $this->helper->generateResetToken($user);

            // Send invitation link
            $email = (new TemplatedEmail())
                ->to($user->getEmail())
                ->subject($this->translator->trans('mail.invite_user.subject', ['app_name' => $this->appName]))
                ->htmlTemplate('invite/email.html.twig')
                ->context([
                    'resetToken' => $resetToken,
                ]);

            $this->mailer->send($email);
        }
    }

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents(): array
    {
        return [
            'kernel.view' => ['onUserCreated', EventPriorities::POST_WRITE],
        ];
    }

    /**
     * Return whether this subscriber applies for the current request.
     *
     * @param ViewEvent $event
     *
     * @return bool
     */
    private function supports(ViewEvent $event): bool
    {
        $user   = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();
        $route  = $event->getRequest()->attributes->get('_route');

        return $user instanceof User
            && Request::METHOD_POST === $method
            && 'api_users_post_collection' === $route
            && null !== $user->getId();
    }
}
