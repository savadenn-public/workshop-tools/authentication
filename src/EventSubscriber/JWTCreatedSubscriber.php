<?php

declare(strict_types=1);

namespace App\EventSubscriber;

use App\Entity\ApiKey;
use App\Entity\User;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTCreatedEvent;

/**
 * Class JWTCreatedSubscriber
 */
class JWTCreatedSubscriber implements EventSubscriberInterface
{
    /**
     * Append user ID in the JWT payload
     *
     * @param JWTCreatedEvent $event
     *
     * @return void
     */
    public function onJWTCreated(JWTCreatedEvent $event): void
    {
        $user    = $event->getUser();
        $payload = $event->getData();

        if ($user instanceof User || $user instanceof ApiKey) {
            $payload['guid'] = $user->getId();
        }

        if ($user instanceof User) {
            foreach ($user->getUserOrganisations() as $userOrganisation) {
                $payload['organisations'][$userOrganisation->getOrganisation()->getId()] = $userOrganisation->getRoles();
            }
        }

        $event->setData($payload);
    }

    public static function getSubscribedEvents(): array
    {
        return [
            'lexik_jwt_authentication.on_jwt_created' => 'onJWTCreated',
        ];
    }
}
