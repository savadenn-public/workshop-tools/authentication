<?php

declare(strict_types=1);

namespace App\OpenApi;

use ApiPlatform\Core\OpenApi\Factory\OpenApiFactoryInterface;
use ApiPlatform\Core\OpenApi\OpenApi;
use ApiPlatform\Core\OpenApi\Model;

/**
 * Add endpoints to SwaggerUI, that will be used
 * by automated processed that needs to eb authenticated
 */
final class ApiTokenDecorator implements OpenApiFactoryInterface
{

    public function __construct(private OpenApiFactoryInterface $decorated)
    {
    }

    public function __invoke(array $context = []): OpenApi
    {
        $openApi = ($this->decorated)($context);

        $this->addSchemas($openApi);

        $this->addApiTokenEndpoint($openApi);

        return $openApi;
    }

    /**
     * Add authentication endpoint
     *
     * @param OpenApi $openApi
     */
    private function addApiTokenEndpoint(OpenApi $openApi): void
    {
        $pathItem = new Model\PathItem(
            ref: 'API Token',
            post: new Model\Operation(
                operationId: 'postApiCredentialsItem',
                tags: ['ApiToken'],
                responses: [
                    '200' => [
                        'description' => 'Get API token',
                        'content' => [
                            'application/json' => [
                                'schema' => [
                                    '$ref' => '#/components/schemas/ApiTokens',
                                ],
                            ],
                        ],
                    ],
                ],
                summary: 'Get API token to login.',
                requestBody: new Model\RequestBody(
                    description: 'Generate new API Token',
                    content: new \ArrayObject([
                        'application/json' => [
                            'schema' => [
                                '$ref' => '#/components/schemas/ApiCredentials',
                            ],
                        ],
                    ]),
                ),
            ),
        );

        $openApi->getPaths()->addPath('/api_token', $pathItem);
    }

    private function addSchemas(OpenApi $openApi): void
    {
        $schemas = $openApi->getComponents()->getSchemas();

        $schemas['ApiCredentials'] = new \ArrayObject([
            'type'       => 'object',
            'properties' => [
                'usage'   => [
                    'type'    => 'string',
                    'example' => 'micro-service 3',
                ],
                'token' => [
                    'type'    => 'string',
                    'example' => 'foo',
                ],
            ],
        ]);

        $schemas['ApiTokens'] = new \ArrayObject([
            'type'       => 'object',
            'properties' => [
                'token' => [
                    'type'     => 'string',
                    'readOnly' => true,
                ],
            ],
        ]);
    }
}
