<?php

declare(strict_types=1);

namespace App\OpenApi;

use ApiPlatform\Core\OpenApi\Factory\OpenApiFactoryInterface;
use ApiPlatform\Core\OpenApi\OpenApi;
use ApiPlatform\Core\OpenApi\Model;

/**
 * Add endpoints to SwaggerUI
 */
final class JwtDecorator implements OpenApiFactoryInterface
{

    public function __construct(private OpenApiFactoryInterface $decorated)
    {
    }

    public function __invoke(array $context = []): OpenApi
    {
        $openApi = ($this->decorated)($context);

        $this->addSchemas($openApi);

        $this->addAuthenticationTokenEndpoint($openApi);
        $this->addRefreshTokenEndpoint($openApi);
        $this->addMercureDiscoveryEndpoint($openApi);

        return $openApi;
    }

    /**
     * Add authentication endpoint
     *
     * @param OpenApi $openApi
     */
    private function addAuthenticationTokenEndpoint(OpenApi $openApi): void
    {
        $pathItem = new Model\PathItem(
            ref: 'JWT Token',
            post: new Model\Operation(
                operationId: 'postCredentialsItem',
                tags: ['Token'],
                responses: [
                    '200' => [
                        'description' => 'Get JWT token',
                        'content'     => [
                            'application/json' => [
                                'schema' => [
                                    '$ref' => '#/components/schemas/Tokens',
                                ],
                            ],
                        ],
                    ],
                ],
                summary: 'Get JWT token to login.',
                requestBody: new Model\RequestBody(
                    description: 'Generate new JWT Token',
                    content: new \ArrayObject([
                        'application/json' => [
                            'schema' => [
                                '$ref' => '#/components/schemas/Credentials',
                            ],
                        ],
                    ]),
                ),
            ),
        );

        $openApi->getPaths()->addPath('/authentication_token', $pathItem);
    }

    /**
     * Add refresh token endpoint
     *
     * @param OpenApi $openApi
     */
    private function addRefreshTokenEndpoint(OpenApi $openApi): void
    {
        $pathRefreshToken = new Model\PathItem(
            ref: 'Refresh Token',
            post: new Model\Operation(
                operationId: 'postRefreshItem',
                tags: ['RefreshToken'],
                responses: [
                    '200' => [
                        'description' => 'Get new JWT token & its refresh token',
                        'content'     => [
                            'application/json' => [
                                'schema' => [
                                    '$ref' => '#/components/schemas/Tokens',
                                ],
                            ],
                        ],
                    ],
                ],
                summary: 'Refresh expired JWT token to login.',
                requestBody: new Model\RequestBody(
                    description: 'Refresh expired JWT token',
                    content: new \ArrayObject([
                        'application/json' => [
                            'schema' => [
                                '$ref' => '#/components/schemas/RefreshCredentials',
                            ],
                        ],
                    ]),
                ),
            ),
        );
        $openApi->getPaths()->addPath('/refresh_token', $pathRefreshToken);
    }

    /**
     * Add Mercure discovery endpoint
     *
     * @param OpenApi $openApi
     */
    private function addMercureDiscoveryEndpoint(OpenApi $openApi): void
    {
        $path = new Model\PathItem(
            ref: 'Get Mercure JWT',
            get: new Model\Operation(
                operationId: 'getMercureJWT',
                tags: ['Mercure'],
                responses: [
                    '200' => [
                        'description' => 'Get new JWT token for Mercure subscriber',
                        'headers'     => [
                            'Set-Cookie' => [
                                'description' => '"mercureAuthorization" cookie containing a valid JWT',
                                'type'        => 'string',
                            ],
                        ],
                    ],
                    '204' => [
                        'description' => 'Subscriber already has a valid JWT',
                    ],
                ],
                summary: 'Get a JWT for Mercure subscriber',
            ),
        );
        $openApi->getPaths()->addPath('/mercure/discovery', $path);
    }

    private function addSchemas(OpenApi $openApi): void
    {
        $schemas = $openApi->getComponents()->getSchemas();

        $schemas['Credentials'] = new \ArrayObject([
            'type'       => 'object',
            'properties' => [
                'email'    => [
                    'type'    => 'string',
                    'example' => 'bob@example.com',
                ],
                'password' => [
                    'type'    => 'string',
                    'example' => 'foo',
                ],
            ],
        ]);

        $schemas['Tokens'] = new \ArrayObject([
            'type'       => 'object',
            'properties' => [
                'token'         => [
                    'type'     => 'string',
                    'readOnly' => true,
                ],
                'refresh_token' => [
                    'type'     => 'string',
                    'readOnly' => true,
                ],
            ],
        ]);

        $schemas['RefreshCredentials'] = new \ArrayObject([
            'type'       => 'object',
            'properties' => [
                'refresh_token' => [
                    'type'    => 'string',
                    'example' => '5da73191fdf52ee4729193231595fe4b6c9c3cafe5b9606e5b741e04ad91ca9143adfc10c3843e8e316ced88830a31c32c4fdcd0914d981a3b9d92417e65ea97',
                ],
            ],
        ]);
    }
}
