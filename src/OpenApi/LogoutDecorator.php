<?php
declare(strict_types=1);

namespace App\OpenApi;

use ApiPlatform\Core\OpenApi\Factory\OpenApiFactoryInterface;
use ApiPlatform\Core\OpenApi\OpenApi;
use ApiPlatform\Core\OpenApi\Model;

/**
 * Class LogoutDecorator
 */
final class LogoutDecorator implements OpenApiFactoryInterface
{
    /**
     * JwtDecorator constructor.
     *
     * @param OpenApiFactoryInterface $decorated
     */
    public function __construct(private OpenApiFactoryInterface $decorated)
    {
    }

    /**
     * @inheritDoc
     */
    public function __invoke(array $context = []): OpenApi
    {
        $openApi = ($this->decorated)($context);

        $this->addLogoutEndpoint($openApi);

        return $openApi;
    }

    /**
     * Add authentication endpoint
     *
     * @param OpenApi $openApi
     */
    private function addLogoutEndpoint(OpenApi $openApi): void
    {
        $pathItem = new Model\PathItem(
            ref: 'Log user out',
            post: new Model\Operation(
                operationId: 'logout',
                tags: ['Logout'],
                responses: [
                    '204' => [
                        'description' => 'Refresh token deleted',
                    ],
                ],
                summary: 'Log user out by removing its refresh token',
                requestBody: new Model\RequestBody(
                    description: 'Log user out by removing its refresh token',
                    content: new \ArrayObject([
                        'application/json' => [
                            'schema' => [
                                'type'       => 'object',
                                'properties' => [
                                    'refresh_token' => [
                                        'type'    => 'string',
                                        'example' => 'sd51f6s5dfgs1dfg65sdfg65432165',
                                    ],
                                ],
                            ],
                        ],
                    ]),
                ),
            ),
        );

        $openApi->getPaths()->addPath('/logout', $pathItem);
    }
}
