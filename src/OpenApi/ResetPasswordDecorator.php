<?php

declare(strict_types=1);

namespace App\OpenApi;

use ApiPlatform\Core\OpenApi\Factory\OpenApiFactoryInterface;
use ApiPlatform\Core\OpenApi\OpenApi;
use ApiPlatform\Core\OpenApi\Model;

/**
 * Class ResetPasswordDecorator
 */
final class ResetPasswordDecorator implements OpenApiFactoryInterface
{
    public function __construct(private OpenApiFactoryInterface $decorated)
    {
    }

    public function __invoke(array $context = []): OpenApi
    {
        $openApi = ($this->decorated)($context);

        $this->addRequestEndpoint($openApi);
        $this->addResetEndpoint($openApi);

        return $openApi;
    }

    /**
     * Add authentication endpoint
     *
     * @param OpenApi $openApi
     */
    private function addRequestEndpoint(OpenApi $openApi): void
    {
        $pathItem = new Model\PathItem(
            ref: 'Request reset password',
            post: new Model\Operation(
                operationId: 'requestPasswordRecovery',
                tags: ['ResetPassword'],
                responses: [
                    '202' => [
                        'description' => 'Recovery link sent by email',
                    ],
                ],
                summary: 'Send a recovery link by email',
                requestBody: new Model\RequestBody(
                    description: 'Send a recovery link by email',
                    content: new \ArrayObject([
                        'application/json' => [
                            'schema' => [
                                'type'       => 'object',
                                'properties' => [
                                    'email' => [
                                        'type'    => 'string',
                                        'example' => 'bob@example.com',
                                    ],
                                ],
                            ],
                        ],
                    ]),
                ),
            ),
        );

        $openApi->getPaths()->addPath('/reset_password', $pathItem);
    }

    /**
     * Add refresh token endpoint
     *
     * @param OpenApi $openApi
     */
    private function addResetEndpoint(OpenApi $openApi): void
    {
        $pathRefreshToken = new Model\PathItem(
            ref: 'Reset password',
            post: new Model\Operation(
                operationId: 'resetPassword',
                tags: ['ResetPassword'],
                responses: [
                    '200' => [
                        'description' => 'Password changed',
                    ],
                    '400' => [
                        'description' => 'Invalid recovery token',
                    ],
                    '422' => [
                        'description' => 'Empty or too short password',
                    ],
                ],
                summary: 'Consume recovery token & change user password',
                parameters: [
                    [
                        'name'            => 'token',
                        'in'              => 'path',
                        'description'     => 'Recovery token',
                        'required'        => true,
                        'allowEmptyValue' => false,
                        'schema'          => [
                            'type' => 'string',
                        ],
                        'style'           => 'simple',
                    ],
                ],
                requestBody: new Model\RequestBody(
                    description: 'The new password of the user',
                    content: new \ArrayObject([
                        'application/json' => [
                            'schema' => [
                                'type'       => 'object',
                                'properties' => [
                                    'password' => [
                                        'type'    => 'string',
                                        'example' => 'n3w p4$$w0rd',
                                    ],
                                ],
                            ],
                        ],
                    ]),
                ),
            ),
        );
        $openApi->getPaths()->addPath('/reset_password/{token}', $pathRefreshToken);
    }
}
