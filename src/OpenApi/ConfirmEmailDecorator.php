<?php

declare(strict_types=1);

namespace App\OpenApi;

use ApiPlatform\Core\OpenApi\Factory\OpenApiFactoryInterface;
use ApiPlatform\Core\OpenApi\OpenApi;
use ApiPlatform\Core\OpenApi\Model;

/**
 * Class ConfirmEmailDecorator
 */
final class ConfirmEmailDecorator implements OpenApiFactoryInterface
{
    public function __construct(private OpenApiFactoryInterface $decorated)
    {
    }

    public function __invoke(array $context = []): OpenApi
    {
        $openApi = ($this->decorated)($context);

        $this->addEndpoint($openApi);

        return $openApi;
    }

    /**
     * Add authentication endpoint
     *
     * @param OpenApi $openApi
     */
    private function addEndpoint(OpenApi $openApi): void
    {
        $pathItem = new Model\PathItem(
            ref: 'Consume token to confirm email change',
            post: new Model\Operation(
                operationId: 'confirm_email_token',
                tags: ['ConfirmEmailToken'],
                responses: [
                    '200' => [
                        'description' => 'User email has been changed.',
                    ],
                    '400' => [
                        'description' => 'Invalid or expired token.',
                    ],
                ],
                summary: 'Consume token to confirm email change',
                requestBody: new Model\RequestBody(
                    description: 'Do confirm confirm new email of a user',
                    content: new \ArrayObject([
                        'application/json' => [
                            'schema' => [
                                'type'       => 'object',
                                'properties' => [
                                    'token' => [
                                        'type'    => 'string',
                                        'example' => 'jhhvKVkgvc+URzF6/rXYw0iiu4/mr+Jkj796DoUYI+3CA=',
                                    ],
                                ],
                            ],
                        ],
                    ]),
                ),
            ),
        );

        $openApi->getPaths()->addPath('/confirm_email', $pathItem);
    }
}
