<?php

declare(strict_types=1);

namespace App\Security\User;

use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;

/**
 * Interface UserPasswordGeneratorInterface
 */
interface UserPasswordGeneratorInterface
{
    /**
     * Generate a random password for user, affect it and return it
     *
     * @param PasswordAuthenticatedUserInterface $user
     *
     * @return string
     */
    public function generateRandomPassword(PasswordAuthenticatedUserInterface $user): string;
}
