<?php

declare(strict_types=1);

namespace App\Security\User;

use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;

/**
 * Class UserPasswordGenerator
 */
class UserPasswordGenerator implements UserPasswordGeneratorInterface
{
    /**
     * @param UserPasswordHasherInterface $hasher
     */
    public function __construct(private UserPasswordHasherInterface $hasher)
    {
    }

    /**
     * @inheritDoc
     */
    public function generateRandomPassword(PasswordAuthenticatedUserInterface $user): string
    {
        $password = bin2hex(random_bytes(16));
        $user->setPassword(
            $this->hasher->hashPassword($user, $password)
        );

        return $password;
    }
}
