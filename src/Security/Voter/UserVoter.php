<?php

declare(strict_types=1);

namespace App\Security\Voter;

use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class UserVoter
 */
class UserVoter extends Voter
{
    public function __construct(private Security $security)
    {
    }

    protected function supports(string $attribute, $subject): bool
    {
        return in_array($attribute, ['CHANGE_PASSWORD', 'USER_EDIT']) && $subject instanceof User;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();
        if (!$user instanceof UserInterface) {
            return false;
        }

        return match ($attribute) {
            'CHANGE_PASSWORD' => $this->isSameResource($subject, $user),
            'USER_EDIT'       => $this->isAdmin() || $this->isSameResource($subject, $user), // A user can only modify himself except if is admin
            default           => false,
        };
    }

    /**
     * Return whether $authenticatedUser is the same resource as given $subject.
     *
     * @param UserInterface $subject
     * @param UserInterface $authenticatedUser
     *
     * @return bool
     */
    private function isSameResource(UserInterface $subject, UserInterface $authenticatedUser): bool
    {
        return $subject === $authenticatedUser;
    }

    /**
     * Return whether current user is an admin
     *
     * @return bool
     */
    private function isAdmin(): bool
    {
        return $this->security->isGranted('ROLE_ADMIN');
    }
}
