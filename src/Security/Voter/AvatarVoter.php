<?php

declare(strict_types=1);

namespace App\Security\Voter;

use App\Entity\Avatar;
use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Voter for Avatar
 */
class AvatarVoter extends Voter
{
    protected function supports(string $attribute, $subject): bool
    {
        return in_array($attribute, ['DELETE', 'PATCH']) && $subject instanceof Avatar;
    }

    /**
     * Make sure user is owner of the resource
     *
     * @param string         $attribute
     * @param Avatar         $subject
     * @param TokenInterface $token
     *
     * @return bool
     */
    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        /** @var User $user */
        $user = $token->getUser();

        if (!$user instanceof UserInterface) {
            return false;
        }

        return $subject->getUser() === $user;
    }
}
