<?php

declare(strict_types=1);

namespace App\ConfirmEmail;

use ApiPlatform\Core\Validator\ValidatorInterface;
use App\Entity\ConfirmEmailToken;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Gesdinet\JWTRefreshTokenBundle\Entity\RefreshToken;

/**
 * Class ConfirmEmailHelper
 */
class ConfirmEmailHelper
{

    /**
     * @param EntityManagerInterface $manager
     * @param ValidatorInterface     $validator
     * @param int                    $tokenLifeTime
     * @param string                 $signingKey
     */
    public function __construct(private EntityManagerInterface $manager, private ValidatorInterface $validator, private int $tokenLifeTime = 86400, private string $signingKey = '')
    {
    }

    /**
     * Generate a confirmation token and persist it if needed
     *
     * @param User   $user
     * @param string $newEmail
     * @param bool   $persist  Persist generated token if true (by default)
     *
     * @return ConfirmEmailToken
     */
    public function generateConfirmToken(User $user, string $newEmail, bool $persist = true): ConfirmEmailToken
    {
        $expiresAt   = new \DateTimeImmutable(sprintf('%d seconds', $this->tokenLifeTime));
        $encodedData = json_encode([$newEmail, $expiresAt->getTimestamp()]);

        $token = $this->manager
            ->getRepository(ConfirmEmailToken::class)
            ->createResetPasswordRequest($user, $expiresAt, $newEmail, $this->getHashedToken($encodedData));

        $this->validator->validate($token);

        if ($persist) {
            $this->manager->persist($token);
            $this->manager->flush();
        }

        return $token;
    }

    /**
     * Validate given token and update user's email
     *
     * @param string $fullToken
     *
     * @return User
     */
    public function validateTokenAndUpdateUser(string $fullToken): User
    {
        $token = $this->manager->getRepository(ConfirmEmailToken::class)->findOneBy(['hashedToken' => $fullToken]);

        if (null === $token) {
            throw new InvalidConfirmEmailTokenException();
        }

        if ($token->isExpired()) {
            throw new ExpiredConfirmEmailTokenException();
        }

        $this->manager->beginTransaction();

        try {
            $user = $token->getUser();
            $user->setEmail($token->getEmail());

            $this->manager->persist($user);
            $this->manager->remove($token);
            $this->manager->flush();
            $this->manager->commit();
        } catch (\Throwable $exception) {
            $this->manager->rollback();

            throw $exception;
        }

        return $user;
    }

    /**
     * Generate a cryptographically secure token with it's non-hashed components.
     *
     * @param string $data Set of data to hash to get a unique token
     *
     * @return string
     */
    private function getHashedToken(string $data): string
    {
        return base64_encode(hash_hmac('sha256', $data, $this->signingKey, true));
    }
}
