<?php

declare(strict_types=1);

namespace App\ConfirmEmail;

use Throwable;

/**
 * Class InvalidConfirmEmailTokenException
 */
class InvalidConfirmEmailTokenException extends ConfirmEmailTokenException
{
    /**
     * @param Throwable|null $previous
     */
    public function __construct(Throwable $previous = null)
    {
        parent::__construct('The confirm email token is invalid. Please try to change your email again.', previous: $previous);
    }
}
