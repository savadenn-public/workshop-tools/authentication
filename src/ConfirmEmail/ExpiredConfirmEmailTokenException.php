<?php

declare(strict_types=1);

namespace App\ConfirmEmail;

use Throwable;

/**
 * Class ExpiredConfirmEmailTokenException
 */
class ExpiredConfirmEmailTokenException extends ConfirmEmailTokenException
{
    /**
     * @param Throwable|null $previous
     */
    public function __construct(Throwable $previous = null)
    {
        parent::__construct('The token is expired. Please change your email again.', previous: $previous);
    }
}
