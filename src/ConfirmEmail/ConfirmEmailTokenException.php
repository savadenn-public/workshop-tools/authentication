<?php

declare(strict_types=1);

namespace App\ConfirmEmail;

/**
 * Class ConfirmEmailTokenException
 */
class ConfirmEmailTokenException extends \Exception
{

}
