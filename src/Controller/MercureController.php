<?php

declare(strict_types=1);

namespace App\Controller;

use Lexik\Bundle\JWTAuthenticationBundle\Services\JWSProvider\JWSProviderInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\Mercure\Jwt\TokenFactoryInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Controller in charge of generating a JWT for Mercure subscribers
 */
#[AsController]
class MercureController
{
    const COOKIE_NAME = 'mercureAuthorization';

    public function __construct(private TokenFactoryInterface $jwtFactory, private JWSProviderInterface $jwsProvider, private LoggerInterface $logger)
    {
    }

    /**
     * Create a JWT for a Mercure subscriber
     *
     * @param Request $request
     *
     * @return Response
     */
    #[Route(path: '/mercure/discovery')]
    public function getJwt(Request $request): Response
    {
        $response = new Response(status: Response::HTTP_NO_CONTENT);

        if (!$this->jwtAlreadyExists($request)) {
            $jwt = $this->jwtFactory->create(['*']);

            $response->setStatusCode(Response::HTTP_OK);
            $response->headers->setCookie(Cookie::create(name: self::COOKIE_NAME, value: $jwt, secure: true, httpOnly: true));
        }

        return $response;
    }

    /**
     * Return whether the Mercure JWT is already set in cookie "mercureAuthorization" and not expired
     *
     * @param Request $request
     *
     * @return bool
     */
    private function jwtAlreadyExists(Request $request): bool
    {
        $exists = false;
        try {
            if ($jwt = $request->cookies->get(self::COOKIE_NAME)) {
                $exists = !$this->jwsProvider->load($jwt)->isExpired();
            }
        } catch (\Throwable $exception) {
            $this->logger->error(sprintf('Error while trying to decode Mercure JWT: %s', $exception->getMessage()));
        }

        return $exists;
    }
}
