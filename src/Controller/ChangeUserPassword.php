<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Controller for changing user password
 */
class ChangeUserPassword
{
    public function __construct(private EntityManagerInterface $entityManager)
    {
    }

    /**
     * @param User $data The hydrated user
     */
    public function __invoke(User $data): void
    {
        $this->entityManager->persist($data);
        $this->entityManager->flush();
    }
}
