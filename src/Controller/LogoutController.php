<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Gesdinet\JWTRefreshTokenBundle\Entity\RefreshToken;
use Gesdinet\JWTRefreshTokenBundle\Model\RefreshTokenManagerInterface;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class LogoutController
 */
class LogoutController
{
    /**
     * @param TokenStorageInterface        $tokenStorage
     * @param RefreshTokenManagerInterface $manager
     * @param EntityManagerInterface       $em
     */
    public function __construct(private TokenStorageInterface $tokenStorage, private RefreshTokenManagerInterface $manager, private EntityManagerInterface $em)
    {
    }

    #[Route('/logout', name: 'app_logout', methods: ['POST'])]
    /**
     * Revoke given refresh_token if exists for the current user
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function revokeRefreshToken(Request $request): JsonResponse
    {
        $token = $request->toArray()['refresh_token'] ?? throw new BadRequestException();
        /** @var User $user */
        $user  = $this->tokenStorage->getToken()->getUser();

        $refreshToken = $this->em->getRepository(RefreshToken::class)->findOneBy([
            'username'     => $user->getId(),
            'refreshToken' => $token,
        ]);

        if (!$refreshToken) {
            throw new BadRequestException();
        }

        $this->manager->delete($refreshToken);

        return new JsonResponse(status: Response::HTTP_NO_CONTENT);
    }
}
