<?php

declare(strict_types=1);

namespace App\DataPersister;

use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use App\ConfirmEmail\ConfirmEmailHelper;
use App\Entity\User;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class UserDataPersister
 */
class UserDataPersister implements ContextAwareDataPersisterInterface
{
    public function __construct(private ContextAwareDataPersisterInterface $decorated, private MailerInterface $mailer, private ConfirmEmailHelper $helper, private TranslatorInterface $translator, private Security $security)
    {
    }

    /**
     * Keep same logic
     *
     * @inheritDoc
     */
    public function supports($data, array $context = []): bool
    {
        return $this->decorated->supports($data, $context);
    }

    /**
     * Override default behavior: don't allow changing email right away, send a confirmation link instead
     *
     * @param User  $data
     * @param array $context
     *
     * @return object|void
     */
    public function persist($data, array $context = [])
    {
        if ($this->needEmailConfirmation($data, $context)) {
            $newEmail = $data->getEmail();
            // Restaure old value
            $data->setEmail($context['previous_data']->getEmail());
            // And send confirmation email
            $this->sendConfirmationLink($data, $newEmail);
        }

        return $this->decorated->persist($data, $context);
    }

    /**
     * Keep same logic
     *
     * @inheritDoc
     */
    public function remove($data, array $context = [])
    {
        return $this->decorated->remove($data, $context);
    }

    /**
     * Return whether email address changed and need a confirmation link to be safely updated.
     * An admin can change email of someone else without confirmation, but still need confirmation
     * for his own email change.
     *
     * @param mixed $data
     * @param array $context
     *
     * @return bool
     */
    private function needEmailConfirmation(mixed $data, array $context): bool
    {
        $operation = $context['item_operation_name'] ?? null;

        return $data instanceof User
            && in_array($operation, ['patch', 'put'])
            && $this->emailChanged($data, $context)
            && (!$this->security->isGranted('ROLE_ADMIN') || $data === $this->security->getUser());
    }

    /**
     * Return whether the User's email changed
     *
     * @param User  $data
     * @param array $context
     *
     * @return bool
     */
    private function emailChanged(User $data, array $context): bool
    {
        return $context['previous_data']->getEmail() !== $data->getEmail();
    }

    /**
     * Send a confirmation link to the user's current email
     *
     * @param User   $user
     * @param string $newEmail
     */
    private function sendConfirmationLink(User $user, string $newEmail): void
    {
        $token = $this->helper->generateConfirmToken($user, $newEmail);

        $email = (new TemplatedEmail())
            ->to($user->getEmail())
            ->subject($this->translator->trans('email.change_email.subject'))
            ->htmlTemplate('confirm_email/confirm_email.html.twig')
            ->context([
                'new_email' => $newEmail,
                'token'     => $token->getHashedToken(),
            ]);

        $this->mailer->send($email);
    }
}
