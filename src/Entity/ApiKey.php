<?php

declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Controller\CreateApiKey;
use App\Repository\ApiKeyRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\IdGenerator\UuidGenerator;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * ApiKey resource
 *
 * Allows authentication of automated processes
 */
#[ORM\Entity(repositoryClass: ApiKeyRepository::class)]
#[UniqueEntity('usage')]
#[ApiResource(
    collectionOperations: [
        'get',
        'post' => [
            'controller'            => CreateApiKey::class,
            'normalization_context' => ['groups' => ['apiKey:create', 'apiKey:read']],
        ],
    ],
    itemOperations: ['get', 'delete'],
    attributes: [
        'normalization_context' => ['groups' => 'apiKey:read'],
        'security'              => "is_granted('ROLE_ADMIN')",
    ],
)]
class ApiKey implements UserInterface, PasswordAuthenticatedUserInterface
{
    #[ORM\Id]
    #[ORM\Column(type: 'uuid')]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: UuidGenerator::class)]
    #[Groups('apiKey:read')]
    private ?string $id = null;

    #[ORM\Column(type: 'string', length: 255, unique: true)]
    #[Assert\NotBlank]
    #[Assert\Length(max: 255)]
    #[Groups('apiKey:read')]
    private string $usage;

    #[ORM\Column(type: 'string', length: 255)]
    #[ApiProperty(readable: false, writable: false)]
    #[Assert\NotBlank]
    private ?string $token = null;

    #[Groups(["apiKey:create"])]
    private ?string $plainToken = null;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getUsage(): string
    {
        return $this->usage;
    }

    public function setUsage(string $usage): self
    {
        $this->usage = $usage;

        return $this;
    }

    #[ApiProperty(readable: false)]
    public function getPassword(): string
    {
        return $this->getToken();
    }

    public function getToken(): string
    {
        return $this->token;
    }

    public function setToken(?string $token): self
    {
        $this->token = $token;

        return $this;
    }

    /**
     * @return string
     *
     * @deprecated since Symfony 5.3, use getUserIdentifier() instead
     */
    #[ApiProperty(readable: false)]
    public function getUsername(): string
    {
        return $this->getUserIdentifier();
    }

    #[ApiProperty(readable: false)]
    public function getUserIdentifier(): string
    {
        return $this->usage;
    }

    public function getRoles(): array
    {
        $roles[] = 'ROLE_API';

        return array_unique($roles);
    }

    #[ApiProperty(readable: false)]
    public function getSalt(): ?string
    {
        return null;
    }

    public function eraseCredentials()
    {
        $this->plainToken = null;
    }

    #[ApiProperty(writable: false)]
    public function setCredentials(?string $plainToken, ?string $token): void
    {
        $this->plainToken = $plainToken;
        $this->setToken($token);
    }

    #[Groups(["apiKey:create"])]
    public function getPlainToken(): ?string
    {
        return $this->plainToken;
    }
}
