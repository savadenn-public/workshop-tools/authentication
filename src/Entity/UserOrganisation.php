<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\UserOrganisationRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\IdGenerator\UuidGenerator;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Resource UserOrganisation
 */
#[ORM\Entity(repositoryClass: UserOrganisationRepository::class)]
#[ORM\UniqueConstraint(columns: ['user_id', 'organisation_id'])]
#[UniqueEntity(['user', 'organisation'])]
class UserOrganisation
{
    use CreatedAtTrait;

    #[ORM\Id]
    #[ORM\Column(type: 'uuid')]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: UuidGenerator::class)]
    private ?string $id = null;

    #[ORM\ManyToOne(targetEntity: Organisation::class, inversedBy: 'userOrganisations')]
    #[ORM\JoinColumn(nullable: false, onDelete: 'CASCADE')]
    private Organisation $organisation;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'userOrganisations')]
    #[ORM\JoinColumn(nullable: false, onDelete: 'CASCADE')]
    private User $user;

    /**
     * Roles of the user in the organisation
     *
     * @var string[]
     */
    #[ORM\Column(type: 'array', nullable: true)]
    private array $roles = [];

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getOrganisation(): Organisation
    {
        return $this->organisation;
    }

    public function setOrganisation(Organisation $organisation): self
    {
        $this->organisation = $organisation;

        return $this;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getRoles(): array
    {
        return $this->roles;
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }
}
