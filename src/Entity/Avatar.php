<?php

declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\AvatarRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\IdGenerator\UuidGenerator;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Avatar configuration of a User
 */
#[ORM\Entity(repositoryClass: AvatarRepository::class)]
#[ApiResource(
    collectionOperations: ['post'],
    itemOperations:[
        'get',
        'patch' => ['security' => 'is_granted("PATCH", object)'],
        'delete' => ['security' => 'is_granted("DELETE", object)'],
    ],
)]
#[UniqueEntity('user')]
class Avatar
{
    const EAR_SIZES    = ['small', 'big'];
    const EARRING_TYPE = ['none', 'small', 'big'];
    const EYE_TYPE     = ['circle', 'oval', 'smile', 'shadow'];
    const EYEBROW_TYPE = ['up', 'down', 'eyelashesUp', 'eyelashesDown'];
    const HAT_TYPE     = ['none', 'beanie', 'turban', 'top'];
    const HAIR_TYPE    = ['thick', 'mohawk', 'feathered', 'bob', 'pixie', 'crew', 'none'];
    const NOSE_TYPE    = ['curved', 'round', 'pointed'];
    const MOUTH_TYPE   = ['surprised', 'laugh', 'smile', 'smirk', 'pucker', 'nervous'];
    const SHIRT_TYPE   = ['crew', 'tee', 'polo'];
    const GLASSES_TYPE = ['none', 'round', 'square', 'pilote'];

    #[ORM\Id]
    #[ORM\Column(type: 'uuid')]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: UuidGenerator::class)]
    private ?string $id = null;

    #[ORM\OneToOne(inversedBy: 'avatar', targetEntity: User::class)]
    #[ORM\JoinColumn(nullable: false)]
    #[ApiProperty(writable: false)]
    #[Assert\NotBlank]
    private UserInterface $user;

    #[ORM\Column(type: 'string', length: 9)]
    #[Assert\CssColor(formats: [Assert\CssColor::HEX_LONG, Assert\CssColor::HEX_LONG_WITH_ALPHA])]
    #[Assert\NotBlank]
    private string $hatColor;

    #[ORM\Column(type: 'string', length: 9)]
    #[Assert\CssColor(formats: [Assert\CssColor::HEX_LONG, Assert\CssColor::HEX_LONG_WITH_ALPHA])]
    #[Assert\NotBlank]
    private string $faceColor;

    #[ORM\Column(type: 'string', length: 9)]
    #[Assert\CssColor(formats: [Assert\CssColor::HEX_LONG, Assert\CssColor::HEX_LONG_WITH_ALPHA])]
    #[Assert\NotBlank]
    private string $hairColor;

    #[ORM\Column(type: 'string', length: 9)]
    #[Assert\CssColor(formats: [Assert\CssColor::HEX_LONG, Assert\CssColor::HEX_LONG_WITH_ALPHA])]
    #[Assert\NotBlank]
    private string $shirtColor;

    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\Choice(choices: self::EAR_SIZES)]
    #[Assert\NotBlank]
    private string $earSize;

    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\Choice(choices: self::EYE_TYPE)]
    #[Assert\NotBlank]
    private string $eyeType;

    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\Choice(choices: self::HAT_TYPE)]
    #[Assert\NotBlank]
    private string $hatType;

    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\Choice(choices: self::HAIR_TYPE)]
    #[Assert\NotBlank]
    private string $hairType;

    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\Choice(choices: self::NOSE_TYPE)]
    #[Assert\NotBlank]
    private string $noseType;

    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\Choice(choices: self::MOUTH_TYPE)]
    #[Assert\NotBlank]
    private string $mouthType;

    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\Choice(choices: self::SHIRT_TYPE)]
    #[Assert\NotBlank]
    private string $shirtType;

    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\Choice(choices: self::GLASSES_TYPE)]
    #[Assert\NotBlank]
    private string $glassesType;

    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\Choice(choices: self::EARRING_TYPE)]
    #[Assert\NotBlank]
    private string $earring;

    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\Choice(choices: self::EYEBROW_TYPE)]
    #[Assert\NotBlank]
    private string $eyebrowType;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getUser(): ?UserInterface
    {
        return $this->user;
    }

    public function setUser(UserInterface $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getHatColor(): ?string
    {
        return $this->hatColor;
    }

    public function setHatColor(string $hatColor): self
    {
        $this->hatColor = $hatColor;

        return $this;
    }

    public function getFaceColor(): ?string
    {
        return $this->faceColor;
    }

    public function setFaceColor(string $faceColor): self
    {
        $this->faceColor = $faceColor;

        return $this;
    }

    public function getHairColor(): ?string
    {
        return $this->hairColor;
    }

    public function setHairColor(string $hairColor): self
    {
        $this->hairColor = $hairColor;

        return $this;
    }

    public function getShirtColor(): ?string
    {
        return $this->shirtColor;
    }

    public function setShirtColor(string $shirtColor): self
    {
        $this->shirtColor = $shirtColor;

        return $this;
    }

    public function getEarSize(): ?string
    {
        return $this->earSize;
    }

    public function setEarSize(string $earSize): self
    {
        $this->earSize = $earSize;

        return $this;
    }

    public function getEyeType(): ?string
    {
        return $this->eyeType;
    }

    public function setEyeType(string $eyeType): self
    {
        $this->eyeType = $eyeType;

        return $this;
    }

    public function getHatType(): ?string
    {
        return $this->hatType;
    }

    public function setHatType(string $hatType): self
    {
        $this->hatType = $hatType;

        return $this;
    }

    public function getHairType(): ?string
    {
        return $this->hairType;
    }

    public function setHairType(string $hairType): self
    {
        $this->hairType = $hairType;

        return $this;
    }

    public function getNoseType(): ?string
    {
        return $this->noseType;
    }

    public function setNoseType(string $noseType): self
    {
        $this->noseType = $noseType;

        return $this;
    }

    public function getMouthType(): ?string
    {
        return $this->mouthType;
    }

    public function setMouthType(string $mouthType): self
    {
        $this->mouthType = $mouthType;

        return $this;
    }

    public function getShirtType(): ?string
    {
        return $this->shirtType;
    }

    public function setShirtType(string $shirtType): self
    {
        $this->shirtType = $shirtType;

        return $this;
    }

    public function getGlassesType(): ?string
    {
        return $this->glassesType;
    }

    public function setGlassesType(string $glassesType): self
    {
        $this->glassesType = $glassesType;

        return $this;
    }

    public function getEarring(): ?string
    {
        return $this->earring;
    }

    public function setEarring(string $earring): self
    {
        $this->earring = $earring;

        return $this;
    }

    public function getEyebrowType(): ?string
    {
        return $this->eyebrowType;
    }

    public function setEyebrowType(string $eyebrowType): self
    {
        $this->eyebrowType = $eyebrowType;

        return $this;
    }
}
