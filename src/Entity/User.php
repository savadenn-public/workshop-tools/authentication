<?php

declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Controller\ChangeUserPassword;
use App\Dto\PasswordChange;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\IdGenerator\UuidGenerator;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * User resource
 */
#[ORM\Entity(repositoryClass: UserRepository::class)]
#[ORM\Table(name: '`user`')]
#[ApiResource(
    collectionOperations: [
        'get',
        'post' => [
            'security'        => 'is_granted("ROLE_ADMIN")',
            'openapi_context' => [
                'requestBody' => [
                    'content' => [
                        'application/json' => [
                            'schema' => [
                                'type'       => 'object',
                                'properties' =>
                                    [
                                        'email' => ['type' => 'string'],
                                        'name'  => ['type' => 'string'],
                                    ],
                            ],
                        ],
                    ],
                ],
                'responses'   => [
                    '201' => [
                        'content' => [
                            'application/json' => [
                                'schema' => [
                                    '$ref' => '#/components/schemas/User',
                                ],
                            ],
                        ],
                    ],
                    '403' => [
                        'content'     => [],
                        "description" => 'Access denied',
                    ],
                ],
            ],
        ],
    ],
    itemOperations: [
        'delete'          => ['security' => 'is_granted("ROLE_ADMIN")'],
        'get',
        'patch'           => [
            'security' => 'is_granted("USER_EDIT", object)',
        ],
        'change_password' => [
            'method'          => 'POST',
            'path'            => '/users/{id}/change_password',
            'controller'      => ChangeUserPassword::class,
            'status'          => 204,
            'input'           => PasswordChange::class,
            'output'          => false,
            'validate'        => false,
            'security'        => 'is_granted("CHANGE_PASSWORD", object)',
            'openapi_context' => [
                'summary'     => 'Changes password of a User resource.',
                'description' => 'Changes password of a User resource.',
                'requestBody' => [
                    'content'     => [
                        'application/json' => [
                            'schema' => [
                                'type'       => 'object',
                                'properties' =>
                                    [
                                        'currentPassword' => ['type' => 'string'],
                                        'newPassword'     => ['type' => 'string'],
                                    ],
                            ],
                        ],
                    ],
                    'description' => 'The current & new password of the User',
                ],
                'responses'   => [
                    "204" => [
                        'content'     => [],
                        "description" => 'Password changed',
                    ],
                    '403' => [
                        'content'     => [],
                        "description" => 'Access denied',
                    ],
                ],
            ],
        ],
    ],
    denormalizationContext: ['groups' => ['user:input']],
)]
#[UniqueEntity("email")]
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    #[ORM\Id]
    #[ORM\Column(type: 'uuid')]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: UuidGenerator::class)]
    private ?string $id = null;

    #[ORM\Column(type: 'string', length: 180, unique: true)]
    #[Assert\NotBlank]
    #[Assert\Email]
    #[Assert\Length(max: 180)]
    #[Groups('user:input')]
    private string $email;

    #[ORM\Column(type: 'json')]
    #[Assert\Choice(choices: ['ROLE_USER', 'ROLE_ADMIN'], multiple: true)]
    #[Groups(['admin:input', 'admin:output'])]
    private array $roles = [];

    /**
     * The hashed password
     */
    #[ORM\Column(type: 'string')]
    #[ApiProperty(readable: false)]
    #[Assert\NotBlank]
    #[Assert\Length(min: 6)]
    private ?string $password = null;

    /**
     * The name the user wants to be called with.
     * Example: Sir, Doctor, Lord Commander, etc
     */
    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Assert\Length(max: 255)]
    #[Groups('user:input')]
    private ?string $name = null;

    /**
     * Date where user account has been locked
     */
    #[ORM\Column(type: 'datetime', nullable: true)]
    #[Groups(['admin:input', 'admin:output'])]
    private ?\DateTimeInterface $lockDate = null;

    #[ORM\OneToOne(mappedBy: 'user', targetEntity: Avatar::class, cascade: ['persist', 'remove'])]
    private ?Avatar $avatar = null;

    /**
     * @var Collection<UserOrganisation>
     */
    #[ORM\OneToMany(mappedBy: 'user', targetEntity: UserOrganisation::class, orphanRemoval: true)]
    #[ApiProperty(readable: false, writable: false)]
    private Collection $userOrganisations;

    public function __construct()
    {
        $this->userOrganisations = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    #[ApiProperty(readable: false)]
    public function getUserIdentifier(): string
    {
        return $this->email;
    }

    #[ApiProperty(readable: false)]
    public function getUsername(): string
    {
        return $this->email;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    #[ApiProperty(readable: false)]
    public function getSalt(): ?string
    {
        return null;
    }

    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getLockDate(): ?\DateTimeInterface
    {
        return $this->lockDate;
    }

    public function setLockDate(?\DateTimeInterface $lockDate): self
    {
        $this->lockDate = $lockDate;

        return $this;
    }

    public function getAvatar(): ?Avatar
    {
        return $this->avatar;
    }

    public function setAvatar(Avatar $avatar): self
    {
        // set the owning side of the relation if necessary
        if ($avatar->getUser() !== $this) {
            $avatar->setUser($this);
        }

        $this->avatar = $avatar;

        return $this;
    }

    /**
     * @return Collection<UserOrganisation>
     */
    public function getUserOrganisations(): Collection
    {
        return $this->userOrganisations;
    }

    public function addUserOrganisation(UserOrganisation $userOrganisation): self
    {
        if (!$this->userOrganisations->contains($userOrganisation)) {
            $this->userOrganisations[] = $userOrganisation;
            $userOrganisation->setUser($this);
        }

        return $this;
    }

    public function removeUserOrganisation(UserOrganisation $userOrganisation): self
    {
        $this->userOrganisations->removeElement($userOrganisation);

        return $this;
    }
}
