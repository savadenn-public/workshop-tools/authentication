<?php

declare(strict_types=1);

namespace App\Command;

use ApiPlatform\Core\Validator\ValidatorInterface;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

#[AsCommand(
    name: 'app:user:create',
    description: 'Create a user',
)]
/**
 * Class UserCreateCommand
 */
class UserCreateCommand extends Command
{
    public function __construct(private EntityManagerInterface $manager, private ValidatorInterface $validator, private UserPasswordHasherInterface $hasher, string $name = null)
    {
        parent::__construct($name);
    }

    /**
     * @inheritDoc
     */
    protected function configure(): void
    {
        $this
            ->addArgument('email', InputArgument::REQUIRED, 'Email of the user')
            ->addOption('password', 'p', InputOption::VALUE_REQUIRED, 'password (if none, one will be randomly generated)')
            ->addOption('isAdmin', 'a', InputOption::VALUE_NONE, 'Whether the user is an admin');
    }

    /**
     * @inheritDoc
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        try {
            $email    = $input->getArgument('email');
            $user     = (new User())->setEmail($email);
            $password = $input->getOption('password');

            if ($input->getOption('isAdmin')) {
                $user->setRoles(['ROLE_ADMIN']);
            }

            if (!$password) {
                $password = bin2hex(random_bytes(16));
            }

            $user->setPassword(
                $this->hasher->hashPassword($user, $password)
            );

            $this->validator->validate($user);

            $this->manager->persist($user);
            $this->manager->flush();
        } catch (\Throwable $exception) {
            $io->error($exception->getMessage());

            return Command::FAILURE;
        }

        $io->success('User created.');
        $io->info($password);

        return Command::SUCCESS;
    }
}
