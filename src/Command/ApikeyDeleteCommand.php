<?php

declare(strict_types=1);

namespace App\Command;

use App\Entity\ApiKey;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'app:apikey:delete',
    description: 'Revoke an ApiKey using its ID',
)]
/**
 * Api Key delete command
 */
class ApikeyDeleteCommand extends Command
{
    public function __construct(private EntityManagerInterface $manager, string $name = null)
    {
        parent::__construct($name);
    }
    protected function configure(): void
    {
        $this
            ->addArgument('id', InputArgument::REQUIRED, 'ID of ApiKey to revoke')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        try {
            $id = $input->getArgument('id');
            $apiKey = $this->manager->getRepository(ApiKey::class)->findOneBy(['id' => $id]);
            if (!$apiKey) {
                $io->error("No API Key found with ID $id");

                return Command::FAILURE;
            }

            $this->manager->remove($apiKey);
            $this->manager->flush();
        } catch (\Throwable $exception) {
            $io->error($exception->getMessage());

            return Command::FAILURE;
        }

        $io->success('API Key revoked successfully');

        return Command::SUCCESS;
    }
}
