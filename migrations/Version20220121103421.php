<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220121103421 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('COMMENT ON COLUMN confirm_email_token.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN confirm_email_token.user_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN reset_password_request.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN reset_password_request.user_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN "user".id IS \'(DC2Type:uuid)\'');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('COMMENT ON COLUMN reset_password_request.id IS NULL');
        $this->addSql('COMMENT ON COLUMN reset_password_request.user_id IS NULL');
        $this->addSql('COMMENT ON COLUMN confirm_email_token.id IS NULL');
        $this->addSql('COMMENT ON COLUMN confirm_email_token.user_id IS NULL');
        $this->addSql('COMMENT ON COLUMN "user".id IS NULL');
    }
}
