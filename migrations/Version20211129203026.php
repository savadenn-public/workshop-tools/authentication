<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Add column `lock_date` to table `user`
 */
final class Version20211129203026 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add column `lock_date` to table `user`';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE "user" ADD lock_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE "user" DROP lock_date');
    }
}
