<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Create table for resource UserOrganisation
 */
final class Version20220327104821 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Create table for resource UserOrganisation';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE user_organisation (id UUID NOT NULL, organisation_id UUID NOT NULL, user_id UUID NOT NULL, roles TEXT DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_662D4EB69E6B1585 ON user_organisation (organisation_id)');
        $this->addSql('CREATE INDEX IDX_662D4EB6A76ED395 ON user_organisation (user_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_662D4EB6A76ED3959E6B1585 ON user_organisation (user_id, organisation_id)');
        $this->addSql('COMMENT ON COLUMN user_organisation.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN user_organisation.organisation_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN user_organisation.user_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN user_organisation.roles IS \'(DC2Type:array)\'');
        $this->addSql('COMMENT ON COLUMN user_organisation.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('ALTER TABLE user_organisation ADD CONSTRAINT FK_662D4EB69E6B1585 FOREIGN KEY (organisation_id) REFERENCES organisation (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_organisation ADD CONSTRAINT FK_662D4EB6A76ED395 FOREIGN KEY (user_id) REFERENCES "user" (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE user_organisation');
    }
}
