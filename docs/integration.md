# Symfony + API Platform integration

::: note
This procedure assumes you're using [Symfony Flex](https://flex.symfony.com/).
:::

## Configuring docker-compose
In your main `docker-compose.yml` file, add the following services & volume:

```yaml
services:
  auth_php:
    image: registry.gitlab.com/savadenn-public/workshop-tools/authentication/php:latest
    depends_on:
      - database # You must have a database service declared, e.g. postgres:13
    command: |
      sh -c '\
          docker-entrypoint bin/console lexik:jwt:generate-keypair --skip-if-exists &&\
          (docker-entrypoint bin/console app:user:create --password foobar -a john@example.com || true) &&\
          docker-entrypoint php-fpm'
    environment:
      APP_ENV: 'prod'
      VUE_APP_URL: https://yourapp.docker.localhost/pwa/#
      CORS_ALLOW_ORIGIN: '^https?://.*(:[0-9]+)?$$'
      TRUSTED_HOSTS: "^(${PROJECT_URL}|caddy)$$"
    volumes:
      - php_socket_auth:/var/run/php
      - "./config/jwt:/srv/api/config/jwt"

  auth_caddy:
    image: registry.gitlab.com/savadenn-public/workshop-tools/authentication/web:latest
    environment:
      SERVER_NAME: "${PROJECT_URL}:80, caddy:80"
      MERCURE_EXTRA_DIRECTIVES: demo /srv/mercure-assets/
      MERCURE_PUBLISHER_JWT_KEY: ${MERCURE_PUBLISHER_JWT_KEY:-!ChangeMe!}
      MERCURE_SUBSCRIBER_JWT_KEY: ${MERCURE_SUBSCRIBER_JWT_KEY:-!ChangeMe!}
    labels:
      - "traefik.enable=true"
      - "traefik.docker.network=localhost_default"
      - "traefik.http.routers.${PROJECT_NAME}-auth.rule=Host(`${PROJECT_URL}`) && PathPrefix(`/auth/`)"
      - "traefik.http.routers.${PROJECT_NAME}-auth.entrypoints=websecure"
    volumes:
      - php_socket_auth:/var/run/php
    networks:
      - default
      - localhost_default

volumes:
  # ...
  php_socket_auth:
```

::: note
An admin user `john@example.com` with password `foobar` will be automatically created when starting auth container.
:::

To be able to validate JWT signature, mount directory containing PEM keys as volume in the PHP service:

```yaml
services:
  php:
    volumes:
      - ./config/jwt:/srv/api/config/jwt
```

## Installing LexikJWTAuthenticationBundle
As authentication is based on JWT, you will require package [lexik/jwt-authentication-bundle](https://github.com/lexik/LexikJWTAuthenticationBundle):

```bash
composer require lexik/jwt-authentication-bundle
```

Add this to your `.gitignore` file:

```.gitignore
###> lexik/jwt-authentication-bundle ###
/config/jwt/*.pem
###< lexik/jwt-authentication-bundle ###
```

## Configuring the Symfony SecurityBundle (database-less)
To work, the provider just needs a few lines of configuration:

```yaml
# config/packages/security.yaml
security:
    providers:
        jwt:
            lexik_jwt: ~
```

Then, use it on your JWT protected firewall:
```yaml
# config/packages/security.yaml
security:
    firewalls:
        main:
            stateless: true
            provider: jwt
            jwt: ~
```

## Documenting the Authentication Mechanism with Swagger/Open API

```yaml
# api/config/packages/api_platform.yaml
api_platform:
    swagger:
        api_keys:
            apiKey:
                name: Authorization
                type: header
```
You're ready to go now!

Sources:

- [API Platform](https://api-platform.com/docs/core/jwt/#jwt-authentication)
- [LexikJWTAuthenticationBundle](https://github.com/lexik/LexikJWTAuthenticationBundle/blob/2.x/Resources/doc/8-jwt-user-provider.md)