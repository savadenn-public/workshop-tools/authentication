# Services

* API 
    * [Endpoint](https://workshop.docker.localhost/auth)
    * [Swagger UI](https://workshop.docker.localhost/auth/docs)
* [Documentation](https://docauth-workshop.docker.localhost)
