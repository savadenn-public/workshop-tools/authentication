# Development

``` include
development/start.md
development/services.md
```

## Build & push PHP image for CI
Some jobs in `.gitlab-ci.yaml` use a custom image of PHP. To manually build it, run the following commands:
```bash
docker build -t registry.gitlab.com/savadenn-public/workshop-tools/authentication/php8.1-ci:latest ./docker/ci
```

And push it on registry:
```bash
docker push registry.gitlab.com/savadenn-public/workshop-tools/authentication/php8.1-ci:latest
```

## Profiling with blackfire.io
This project uses [blackfire](https://blackfire.io/) to monitor performances.

It implies creating an account on https://blackfire.io/. This will lead you getting [credentials](https://blackfire.io/my/settings/credentials).

To enable profiling, fill up file `.env.local` with your server credentials:

```dotenv
BLACKFIRE_SERVER_ID=xxx-xxx-xxx-xxx-xxx
BLACKFIRE_SERVER_TOKEN=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
```

Next, install browser extension [Blackfire Companion](https://addons.mozilla.org/fr/firefox/addon/blackfire/).

Finally, when you want to profile a page, click on the Companion icon and Profile!
